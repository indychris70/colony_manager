== README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...


Please feel free to use a different markup language if you do not plan to run
<tt>rake doc:app</tt>.

Create a version of this app for customer:

1) Have the customer create a Heroku account.
2) Add the customer account as collaborator on Heroku
3) Save and push all changes to warm-taiga-6950
4) Login as customer on dev box terminal.
5) > heroku fork --from warm-taiga-6950 --to colonymanager-<customer>
5.5) At this point you can go to https://colonymanager-<customer>.herokuapp.com and see the app
6) > git clone git@heroku.com:colonymanager-<customer>.git colonymanager-<customer>
7) Copy contents of catmap2 directory (excluding git directory)
8) > git:remote -a colonymanager-<customer>
9) > git branch --unset-upstream
10) test by making a change and pushing.

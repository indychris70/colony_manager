class CreateNotes < ActiveRecord::Migration[4.2]
  def change
    create_table :notes do |t|
      t.string :note_text
      t.references :user, index: true
      t.references :colony, index: true

      t.timestamps
    end
  end
end

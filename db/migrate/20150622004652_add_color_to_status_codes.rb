class AddColorToStatusCodes < ActiveRecord::Migration[4.2]
  def change
    add_column :status_codes, :color, :string
  end
end

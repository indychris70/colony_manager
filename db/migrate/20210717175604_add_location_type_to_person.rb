class AddLocationTypeToPerson < ActiveRecord::Migration[6.1]
  def change
    add_column :people, :location_type, :string
  end
end

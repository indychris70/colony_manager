class AddCreatedAndDeletedColumnsToUsers < ActiveRecord::Migration[4.2]
  def change
    add_column :users, :created_by, :string
    add_column :users, :deleted, :boolean
    add_column :users, :deleted_by, :string
    add_column :users, :deleted_at, :datetime
  end
end

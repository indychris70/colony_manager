class RemoveColumnsFromColony < ActiveRecord::Migration[6.1]
  def change
    remove_column :colonies, :first_name, :string
    remove_column :colonies, :last_name, :string
    remove_column :colonies, :phone, :string
    remove_column :colonies, :email, :string
    remove_column :colonies, :primary_phone_description, :string
    remove_column :colonies, :alt1_phone_desc, :string
    remove_column :colonies, :alt1_phone, :string
    remove_column :colonies, :primary_email_desc, :string
    remove_column :colonies, :alt1_email_desc, :string
    remove_column :colonies, :alt1_email, :string
    remove_column :colonies, :alt2_phone, :string
    remove_column :colonies, :alt2_phone_desc, :string
    remove_column :colonies, :home_address1, :string
    remove_column :colonies, :home_address2, :string
    remove_column :colonies, :home_city, :string
    remove_column :colonies, :home_state, :string
    remove_column :colonies, :home_zip, :string
  end
end

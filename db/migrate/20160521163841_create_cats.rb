class CreateCats < ActiveRecord::Migration[4.2]
  def change
    create_table :cats do |t|
      t.string :name
      t.string :id_code
      t.string :sex
      t.integer :weight_lbs
      t.integer :weight_oz
      t.date :dob
      t.boolean :dob_certainty
      t.boolean :eartip
      t.text :physical_description
      t.text :medical_info
      t.references :colony, index: true

      t.timestamps
    end
  end
end

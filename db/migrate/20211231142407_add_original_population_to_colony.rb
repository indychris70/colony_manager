class AddOriginalPopulationToColony < ActiveRecord::Migration[6.1]
  def change
    add_column :colonies, :original_population, :integer
  end
end

class CreateClinics < ActiveRecord::Migration[6.1]
  def change
    create_table :clinics do |t|
      t.string :name
      t.string :contact1
      t.string :contact2
      t.string :phone1
      t.string :phone2
      t.string :email1
      t.string :email2
      t.boolean :active
      t.boolean :deleted
      t.boolean :deleted_at
      t.string :deleted_by
      t.string :address1
      t.string :address2
      t.string :city
      t.string :state
      t.string :zip_code
      t.text :description

      t.timestamps
    end
  end
end

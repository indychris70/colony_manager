class AddDeleteReasonToCats < ActiveRecord::Migration[4.2]
  def change
    add_column :cats, :delete_reason, :string
  end
end

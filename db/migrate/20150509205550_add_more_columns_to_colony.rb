class AddMoreColumnsToColony < ActiveRecord::Migration[4.2]
  def change
    add_column :colonies, :neighborhood, :string
    add_column :colonies, :county, :string
    add_column :colonies, :township, :string
    add_column :colonies, :formatted_address, :string
  end
end

class AddCatReferencesToNotes < ActiveRecord::Migration[4.2]
  def change
    add_reference :notes, :cat, index: true
  end
end

class AddUserReferencesToColony < ActiveRecord::Migration[4.2]
  def change
    add_reference :colonies, :user, index: true
  end
end

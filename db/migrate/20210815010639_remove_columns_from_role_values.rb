class RemoveColumnsFromRoleValues < ActiveRecord::Migration[6.1]
  def change
    remove_column :role_values, :created_by
    remove_column :role_values, :deleted_by
  end
end

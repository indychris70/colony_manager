class AddColumnsToColony < ActiveRecord::Migration[4.2]
  def change
    add_column :colonies, :primary_phone_description, :string
    add_column :colonies, :alt1_phone_desc, :string
    add_column :colonies, :alt1_phone, :string
    add_column :colonies, :primary_email_desc, :string
    add_column :colonies, :alt1_email_desc, :string
    add_column :colonies, :alt1_email, :string
  end
end

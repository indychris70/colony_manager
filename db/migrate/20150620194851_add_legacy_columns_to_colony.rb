class AddLegacyColumnsToColony < ActiveRecord::Migration[4.2]
  def change
    add_column :colonies, :ins_date, :date
    add_column :colonies, :legacy_people_id, :integer
    add_column :colonies, :legacy_colony_id, :integer
    add_column :colonies, :alt2_phone, :string
    add_column :colonies, :alt2_phone_desc, :string
    add_column :colonies, :home_address1, :string
    add_column :colonies, :home_address2, :string
    add_column :colonies, :home_city, :string
    add_column :colonies, :home_state, :string
    add_column :colonies, :home_zip, :integer
  end
end

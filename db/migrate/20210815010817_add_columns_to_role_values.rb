class AddColumnsToRoleValues < ActiveRecord::Migration[6.1]
  def change
    add_column :role_values, :created_by, :integer
    add_column :role_values, :deleted_by, :integer
  end
end

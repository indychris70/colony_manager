class CreateColonyPopulationEventTypes < ActiveRecord::Migration[6.1]
  def change
    create_table :colony_population_event_types do |t|
      t.string :name
      t.string :description
      t.boolean :is_active
      t.datetime :deleted_at
      t.boolean :is_deleted
      t.integer :created_by
      t.integer :deleted_by
      t.boolean :is_system_type

      t.timestamps
    end
  end
end

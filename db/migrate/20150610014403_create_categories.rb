class CreateCategories < ActiveRecord::Migration[4.2]
  def change
    create_table :categories do |t|
      t.string :name
      t.string :description
      t.boolean :deleted
      t.string :deleted_by
      t.datetime :deleted_at

      t.timestamps
    end
  end
end

class AddBusinessNameToColonies < ActiveRecord::Migration[4.2]
  def change
    add_column :colonies, :business_name, :string
  end
end

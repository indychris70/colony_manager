class CreateRoleValues < ActiveRecord::Migration[6.1]
  def change
    create_table :role_values do |t|
      t.string :name
      t.string :description
      t.boolean :is_active
      t.string :created_by
      t.string :deleted_by
      t.datetime :deleted_at
      t.boolean :deleted

      t.timestamps
    end
  end
end

class AddReferencesToNotes < ActiveRecord::Migration[6.1]
  def change
    add_reference :notes, :trapping_project, foreign_key: true
    add_reference :notes, :cat_event, foreign_key: true
  end
end

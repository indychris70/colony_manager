class MakeUserEmailNotNullAndAddDefault < ActiveRecord::Migration[6.1]
  def change
    change_column_null :users, :email, false, false 
    change_column_default :users, :email, ""
  end
end

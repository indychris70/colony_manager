class CreateColonies < ActiveRecord::Migration[4.2]
  def change
    create_table :colonies do |t|
      t.string :first_name
      t.string :last_name
      t.string :address1
      t.string :address2
      t.string :city
      t.string :state
      t.integer :zip
      t.integer :phone
      t.string :email
      t.integer :num_cats
      t.float :latitude
      t.float :longitude

      t.timestamps
    end
  end
end

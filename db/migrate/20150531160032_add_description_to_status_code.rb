class AddDescriptionToStatusCode < ActiveRecord::Migration[4.2]
  def change
    add_column :status_codes, :description, :string
  end
end

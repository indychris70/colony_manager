class AddPersonIdToCategorizations < ActiveRecord::Migration[6.1]
  def change
    add_column :categorizations, :person_id, :integer
  end
end

class CreateStatusCodes < ActiveRecord::Migration[4.2]
  def change
    create_table :status_codes do |t|
      t.string :status_code
      t.boolean :is_active
      t.string :created_by
      t.string :deleted_by
      t.datetime :deleted_at

      t.timestamps
    end
  end
end

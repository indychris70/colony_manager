class ChangeDatatypeForNoteTextFromStringToText < ActiveRecord::Migration[4.2]
  def up
  	change_column :notes, :note_text, :text, :limit => nil
  end

  def down
  	change_column :notes, :note_text, :string 
  end
end

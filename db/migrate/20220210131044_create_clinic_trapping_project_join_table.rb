class CreateClinicTrappingProjectJoinTable < ActiveRecord::Migration[6.1]
  def change
    create_join_table :clinics, :trapping_projects do |t|
      t.index :clinic_id
      t.index :trapping_project_id
    end
  end
end

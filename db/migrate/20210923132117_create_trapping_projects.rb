class CreateTrappingProjects < ActiveRecord::Migration[6.1]
  def change
    create_table :trapping_projects do |t|
      t.date :scheduled_date
      t.date :actual_date
      t.integer :planned_cats
      t.integer :actual_cats
      t.string :assigned_user
      t.references :user, foreign_key: true
      t.references :colony, foreign_key: true

      t.timestamps
    end
  end
end

class AddTotalAlteredToColonyPopulationEvent < ActiveRecord::Migration[6.1]
  def change
    add_column :colony_pop_events, :total_altered, :integer, :default => 0
  end
end

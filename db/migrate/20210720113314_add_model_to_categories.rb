class AddModelToCategories < ActiveRecord::Migration[6.1]
  def change
    add_column :categories, :model, :string
  end
end

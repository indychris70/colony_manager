class AddDeletedColumnToStatusCodes < ActiveRecord::Migration[4.2]
  def change
    add_column :status_codes, :deleted, :boolean
  end
end

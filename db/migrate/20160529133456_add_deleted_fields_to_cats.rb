class AddDeletedFieldsToCats < ActiveRecord::Migration[4.2]
  def change
    add_column :cats, :deleted, :boolean
    add_column :cats, :deleted_by, :string
    add_column :cats, :deleted_at, :datetime
  end
end

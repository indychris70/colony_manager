class AddClinicReferencesToCatEvent < ActiveRecord::Migration[6.1]
  def change
    add_reference :cat_events, :clinic, foreign_key: true
  end
end

class AddRemoveCatToCatEventOutcomes < ActiveRecord::Migration[6.1]
  def change
    add_column :cat_event_outcomes, :remove_cat, :boolean
  end
end

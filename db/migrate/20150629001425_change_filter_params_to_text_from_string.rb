class ChangeFilterParamsToTextFromString < ActiveRecord::Migration[4.2]
    def up
  	change_column :saved_filters, :filter_params, :text, :limit => nil
  end

  def down
  	change_column :saved_filters, :filter_params, :string 
  end
end

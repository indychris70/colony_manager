class AddCreatedAndDeletedColumnsToColonies < ActiveRecord::Migration[4.2]
  def change
    add_column :colonies, :created_by, :string
    add_column :colonies, :deleted_by, :string
    add_column :colonies, :deleted, :boolean
    add_column :colonies, :deleted_at, :datetime
  end
end

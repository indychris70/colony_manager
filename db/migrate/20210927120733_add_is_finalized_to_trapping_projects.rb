class AddIsFinalizedToTrappingProjects < ActiveRecord::Migration[6.1]
  def change
    add_column :trapping_projects, :is_finalized, :boolean
  end
end

class AddRoleValueReferencesToRole < ActiveRecord::Migration[6.1]
  def change
    add_reference :roles, :role_value, index: false
  end
end

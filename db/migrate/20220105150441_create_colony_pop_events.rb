class CreateColonyPopEvents < ActiveRecord::Migration[6.1]
  def change
    create_table :colony_pop_events do |t|
      t.integer :event_inflow
      t.integer :event_outflow
      t.integer :total_inflow
      t.integer :total_outflow
      t.integer :altered_population
      t.references :colony, foreign_key: true
      t.references :trapping_project, foreign_key: true
      t.references :colony_population_event_type, foreign_key: true

      t.timestamps
    end
  end
end

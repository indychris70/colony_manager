class AddClinicReferencesToNotes < ActiveRecord::Migration[6.1]
  def change
    add_reference :notes, :clinic, foreign_key: true
  end
end

class AddStatusAndAssignedUserToColonyPopEvent < ActiveRecord::Migration[6.1]
  def change
    add_reference :colony_pop_events, :user, foreign_key: true
    add_reference :colony_pop_events, :status_code, foreign_key: true
    add_column :colony_pop_events, :description, :string
  end
end

class AddDefaultValuesToBooleans < ActiveRecord::Migration[6.1]
  def change
    change_column :trapping_projects, :is_finalized, :boolean, :default => false
    change_column :cat_event_outcomes, :is_deleted, :boolean, :default => false
    change_column :cat_event_outcomes, :is_system_outcome, :boolean, :default => false
    change_column :cat_event_types, :is_active, :boolean, :default => false
    change_column :cat_event_types, :is_deleted, :boolean, :default => false
    change_column :cat_event_types, :is_system_type, :boolean, :default => false
    change_column :categories, :deleted, :boolean, :default => false
    change_column :cats, :dob_certainty, :boolean, :default => false
    change_column :cats, :eartip, :boolean, :default => false
    change_column :cats, :deleted, :boolean, :default => false
    change_column :colonies, :gmaps, :boolean, :default => false
    change_column :colonies, :deleted, :boolean, :default => false
    change_column :deleted_cat_reasons, :is_active, :boolean, :default => false
    change_column :notes, :deleted, :boolean, :default => false
    change_column :people, :deleted, :boolean, :default => false
    change_column :role_values, :is_active, :boolean, :default => false
    change_column :role_values, :deleted, :boolean, :default => false
    change_column :status_codes, :is_active, :boolean, :default => false
    change_column :status_codes, :deleted, :boolean, :default => false
    change_column :users, :admin, :boolean, :default => false
    change_column :users, :deleted, :boolean, :default => false
    change_column :users, :field_volunteer, :boolean, :default => false
  end
end

class CreateCatEventTypes < ActiveRecord::Migration[6.1]
  def change
    create_table :cat_event_types do |t|
      t.string :type_name
      t.string :type_description
      t.boolean :is_active
      t.datetime :deleted_at
      t.boolean :is_deleted
      t.integer :created_by
      t.integer :deleted_by
      t.boolean :is_system_type

      t.timestamps
    end
  end
end

class AddColumnsToPeople < ActiveRecord::Migration[6.1]
  def change
    add_column :people, :neighborhood, :string
    add_column :people, :county, :string
    add_column :people, :township, :string
    add_column :people, :formatted_address, :string
  end
end

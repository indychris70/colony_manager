class AddLocationTypeToColony < ActiveRecord::Migration[4.2]
  def change
    add_column :colonies, :location_type, :string
  end
end

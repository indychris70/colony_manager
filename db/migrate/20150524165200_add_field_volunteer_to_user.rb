class AddFieldVolunteerToUser < ActiveRecord::Migration[4.2]
  def change
    add_column :users, :field_volunteer, :boolean, :default => false
  end
end

class AddIsSystemTypeToRoleValue < ActiveRecord::Migration[6.1]
  def change
    add_column :role_values, :is_system_type, :boolean, :default => false
  end
end

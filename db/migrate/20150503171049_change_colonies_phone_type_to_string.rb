class ChangeColoniesPhoneTypeToString < ActiveRecord::Migration[4.2]
  def change
  	change_column :colonies, :phone, :string
  end
end

class CreatePeople < ActiveRecord::Migration[6.1]
  def change
    create_table :people do |t|
      t.string :first_name
      t.string :last_name
      t.string :address1
      t.string :address2
      t.string :city
      t.string :state
      t.string :zip_code
      t.string :phone
      t.string :phone_description
      t.string :email
      t.string :email_description
      t.string :alt1_phone
      t.string :alt1_phone_desc
      t.string :alt1_email
      t.string :alt1_email_desc
      t.string :alt2_phone
      t.string :alt2_phone_desc
      t.boolean :deleted
      t.datetime :deleted_at
      t.float :latitude
      t.float :longitude

      t.timestamps
    end
  end
end

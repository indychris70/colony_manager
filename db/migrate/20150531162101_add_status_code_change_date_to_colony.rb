class AddStatusCodeChangeDateToColony < ActiveRecord::Migration[4.2]
  def change
    add_column :colonies, :status_code_change_date, :datetime
  end
end

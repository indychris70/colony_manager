class CreateSavedFilters < ActiveRecord::Migration[4.2]
  def change
    create_table :saved_filters do |t|
      t.references :user, index: true
      t.string :filter_params
      t.string :name

      t.timestamps
    end
  end
end

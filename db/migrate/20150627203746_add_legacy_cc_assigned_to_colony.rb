class AddLegacyCcAssignedToColony < ActiveRecord::Migration[4.2]
  def change
    add_column :colonies, :legacy_cc_assigned, :integer
  end
end

class ChangeColoniesColumnNameZipToZipCode < ActiveRecord::Migration[5.2]
  def change
    rename_column :colonies, :zip, :zip_code
  end
end

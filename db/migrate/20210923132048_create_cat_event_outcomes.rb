class CreateCatEventOutcomes < ActiveRecord::Migration[6.1]
  def change
    create_table :cat_event_outcomes do |t|
      t.string :name
      t.string :description
      t.boolean :is_active
      t.datetime :deleted_at
      t.boolean :is_deleted
      t.integer :created_by
      t.integer :deleted_by
      t.boolean :is_system_outcome

      t.timestamps
    end
  end
end

class AddDeletedColumnsToNotes < ActiveRecord::Migration[4.2]
  def change
    add_column :notes, :deleted_by, :string
    add_column :notes, :deleted, :boolean
    add_column :notes, :deleted_at, :datetime
  end
end

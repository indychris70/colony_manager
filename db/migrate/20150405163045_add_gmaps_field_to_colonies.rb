class AddGmapsFieldToColonies < ActiveRecord::Migration[4.2]
  def change
    add_column :colonies, :gmaps, :boolean
  end
end

class AddColonyNameToColonies < ActiveRecord::Migration[6.1]
  def change
    add_column :colonies, :colony_name, :string
  end
end

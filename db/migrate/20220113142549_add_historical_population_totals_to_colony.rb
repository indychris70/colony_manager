class AddHistoricalPopulationTotalsToColony < ActiveRecord::Migration[6.1]
  def change
    add_column :colonies, :total_inflow, :integer, :default => 0
    add_column :colonies, :total_outflow, :integer, :default => 0
    add_column :colonies, :total_altered, :integer, :default => 0
  end
end

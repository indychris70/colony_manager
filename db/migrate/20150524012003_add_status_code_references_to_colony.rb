class AddStatusCodeReferencesToColony < ActiveRecord::Migration[4.2]
  def change
    add_reference :colonies, :status_code, index: true
  end
end

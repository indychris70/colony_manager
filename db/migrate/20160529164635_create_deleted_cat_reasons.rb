class CreateDeletedCatReasons < ActiveRecord::Migration[4.2]
  def change
    create_table :deleted_cat_reasons do |t|
      t.string :reason
      t.string :description
      t.boolean :is_active
      t.string :created_by

      t.timestamps null: false
    end
  end
end

class CreateCatEvents < ActiveRecord::Migration[6.1]
  def change
    create_table :cat_events do |t|
      t.references :trapping_project, foreign_key: true
      t.references :cat, foreign_key: true
      t.references :cat_event_type, foreign_key: true
      t.references :cat_event_outcome, foreign_key: true
      t.date :start_date
      t.date :end_date

      t.timestamps
    end
  end
end

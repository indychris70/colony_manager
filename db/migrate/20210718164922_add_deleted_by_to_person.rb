class AddDeletedByToPerson < ActiveRecord::Migration[6.1]
  def change
    add_column :people, :deleted_by, :string
  end
end

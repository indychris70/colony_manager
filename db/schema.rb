# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_04_07_122309) do

  create_table "cat_event_outcomes", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.boolean "is_active"
    t.datetime "deleted_at"
    t.boolean "is_deleted", default: false
    t.integer "created_by"
    t.integer "deleted_by"
    t.boolean "is_system_outcome", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "remove_cat"
  end

  create_table "cat_event_types", force: :cascade do |t|
    t.string "type_name"
    t.string "type_description"
    t.boolean "is_active", default: false
    t.datetime "deleted_at"
    t.boolean "is_deleted", default: false
    t.integer "created_by"
    t.integer "deleted_by"
    t.boolean "is_system_type", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "cat_events", force: :cascade do |t|
    t.integer "trapping_project_id"
    t.integer "cat_id"
    t.integer "cat_event_type_id"
    t.integer "cat_event_outcome_id"
    t.date "start_date"
    t.date "end_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "clinic_id"
    t.index ["cat_event_outcome_id"], name: "index_cat_events_on_cat_event_outcome_id"
    t.index ["cat_event_type_id"], name: "index_cat_events_on_cat_event_type_id"
    t.index ["cat_id"], name: "index_cat_events_on_cat_id"
    t.index ["clinic_id"], name: "index_cat_events_on_clinic_id"
    t.index ["trapping_project_id"], name: "index_cat_events_on_trapping_project_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.boolean "deleted", default: false
    t.string "deleted_by"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "model"
  end

  create_table "categorizations", force: :cascade do |t|
    t.integer "colony_id"
    t.integer "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "person_id"
    t.index ["category_id"], name: "index_categorizations_on_category_id"
    t.index ["colony_id"], name: "index_categorizations_on_colony_id"
  end

  create_table "cats", force: :cascade do |t|
    t.string "name"
    t.string "id_code"
    t.string "sex"
    t.integer "weight_lbs"
    t.integer "weight_oz"
    t.date "dob"
    t.boolean "dob_certainty", default: false
    t.boolean "eartip", default: false
    t.text "physical_description"
    t.text "medical_info"
    t.integer "colony_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean "deleted", default: false
    t.string "deleted_by"
    t.datetime "deleted_at"
    t.string "delete_reason"
    t.index ["colony_id"], name: "index_cats_on_colony_id"
  end

  create_table "clinics", force: :cascade do |t|
    t.string "name"
    t.string "contact1"
    t.string "contact2"
    t.string "phone1"
    t.string "phone2"
    t.string "email1"
    t.string "email2"
    t.boolean "active"
    t.boolean "deleted"
    t.boolean "deleted_at"
    t.string "deleted_by"
    t.string "address1"
    t.string "address2"
    t.string "city"
    t.string "state"
    t.string "zip_code"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "clinics_trapping_projects", id: false, force: :cascade do |t|
    t.integer "clinic_id", null: false
    t.integer "trapping_project_id", null: false
    t.index ["clinic_id"], name: "index_clinics_trapping_projects_on_clinic_id"
    t.index ["trapping_project_id"], name: "index_clinics_trapping_projects_on_trapping_project_id"
  end

  create_table "colonies", force: :cascade do |t|
    t.string "address1"
    t.string "address2"
    t.string "city"
    t.string "state"
    t.integer "zip_code"
    t.integer "num_cats"
    t.float "latitude"
    t.float "longitude"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean "gmaps", default: false
    t.string "business_name"
    t.string "neighborhood"
    t.string "county"
    t.string "township"
    t.string "formatted_address"
    t.string "created_by"
    t.string "deleted_by"
    t.boolean "deleted", default: false
    t.datetime "deleted_at"
    t.integer "status_code_id"
    t.integer "user_id"
    t.datetime "status_code_change_date"
    t.date "ins_date"
    t.integer "legacy_people_id"
    t.integer "legacy_colony_id"
    t.string "location_type"
    t.integer "legacy_cc_assigned"
    t.string "colony_name"
    t.integer "original_population"
    t.integer "total_inflow", default: 0
    t.integer "total_outflow", default: 0
    t.integer "total_altered", default: 0
    t.index ["status_code_id"], name: "index_colonies_on_status_code_id"
    t.index ["user_id"], name: "index_colonies_on_user_id"
  end

  create_table "colony_pop_events", force: :cascade do |t|
    t.integer "event_inflow"
    t.integer "event_outflow"
    t.integer "total_inflow"
    t.integer "total_outflow"
    t.integer "altered_population"
    t.integer "colony_id"
    t.integer "trapping_project_id"
    t.integer "colony_population_event_type_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "total_altered", default: 0
    t.integer "event_altered", default: 0
    t.integer "user_id"
    t.integer "status_code_id"
    t.string "description"
    t.index ["colony_id"], name: "index_colony_pop_events_on_colony_id"
    t.index ["colony_population_event_type_id"], name: "index_colony_pop_events_on_colony_population_event_type_id"
    t.index ["status_code_id"], name: "index_colony_pop_events_on_status_code_id"
    t.index ["trapping_project_id"], name: "index_colony_pop_events_on_trapping_project_id"
    t.index ["user_id"], name: "index_colony_pop_events_on_user_id"
  end

  create_table "colony_population_event_types", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.boolean "is_active"
    t.datetime "deleted_at"
    t.boolean "is_deleted"
    t.integer "created_by"
    t.integer "deleted_by"
    t.boolean "is_system_type"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "deleted_cat_reasons", force: :cascade do |t|
    t.string "reason"
    t.string "description"
    t.boolean "is_active", default: false
    t.string "created_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "notes", force: :cascade do |t|
    t.text "note_text"
    t.integer "user_id"
    t.integer "colony_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "deleted_by"
    t.boolean "deleted", default: false
    t.datetime "deleted_at"
    t.integer "cat_id"
    t.integer "trapping_project_id"
    t.integer "cat_event_id"
    t.integer "clinic_id"
    t.index ["cat_event_id"], name: "index_notes_on_cat_event_id"
    t.index ["cat_id"], name: "index_notes_on_cat_id"
    t.index ["clinic_id"], name: "index_notes_on_clinic_id"
    t.index ["colony_id"], name: "index_notes_on_colony_id"
    t.index ["trapping_project_id"], name: "index_notes_on_trapping_project_id"
    t.index ["user_id"], name: "index_notes_on_user_id"
  end

  create_table "people", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "address1"
    t.string "address2"
    t.string "city"
    t.string "state"
    t.string "zip_code"
    t.string "phone"
    t.string "phone_description"
    t.string "email"
    t.string "email_description"
    t.string "alt1_phone"
    t.string "alt1_phone_desc"
    t.string "alt1_email"
    t.string "alt1_email_desc"
    t.string "alt2_phone"
    t.string "alt2_phone_desc"
    t.boolean "deleted", default: false
    t.datetime "deleted_at"
    t.float "latitude"
    t.float "longitude"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "neighborhood"
    t.string "county"
    t.string "township"
    t.string "formatted_address"
    t.string "location_type"
    t.string "deleted_by"
    t.string "full_name"
  end

  create_table "role_values", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.boolean "is_active", default: false
    t.datetime "deleted_at"
    t.boolean "deleted", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "created_by"
    t.integer "deleted_by"
    t.boolean "is_system_type", default: false
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.integer "colony_id"
    t.integer "person_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "role_value_id"
    t.index ["colony_id"], name: "index_roles_on_colony_id"
    t.index ["person_id"], name: "index_roles_on_person_id"
  end

  create_table "saved_filters", force: :cascade do |t|
    t.integer "user_id"
    t.text "filter_params"
    t.string "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["user_id"], name: "index_saved_filters_on_user_id"
  end

  create_table "status_codes", force: :cascade do |t|
    t.string "status_code"
    t.boolean "is_active", default: false
    t.string "created_by"
    t.string "deleted_by"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean "deleted", default: false
    t.string "description"
    t.string "color"
  end

  create_table "trapping_projects", force: :cascade do |t|
    t.date "scheduled_date"
    t.date "actual_date"
    t.integer "planned_cats"
    t.integer "actual_cats"
    t.string "assigned_user"
    t.integer "user_id"
    t.integer "colony_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "is_finalized", default: false
    t.index ["colony_id"], name: "index_trapping_projects_on_colony_id"
    t.index ["user_id"], name: "index_trapping_projects_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email", default: "", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean "admin", default: false
    t.string "created_by"
    t.boolean "deleted", default: false
    t.string "deleted_by"
    t.datetime "deleted_at"
    t.boolean "field_volunteer", default: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer "invitation_limit"
    t.string "invited_by_type"
    t.integer "invited_by_id"
    t.integer "invitations_count", default: 0
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["invitation_token"], name: "index_users_on_invitation_token", unique: true
    t.index ["invited_by_id"], name: "index_users_on_invited_by_id"
    t.index ["invited_by_type", "invited_by_id"], name: "index_users_on_invited_by"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "cat_events", "cat_event_outcomes"
  add_foreign_key "cat_events", "cat_event_types"
  add_foreign_key "cat_events", "cats"
  add_foreign_key "cat_events", "clinics"
  add_foreign_key "cat_events", "trapping_projects"
  add_foreign_key "colony_pop_events", "colonies"
  add_foreign_key "colony_pop_events", "colony_population_event_types"
  add_foreign_key "colony_pop_events", "status_codes"
  add_foreign_key "colony_pop_events", "trapping_projects"
  add_foreign_key "colony_pop_events", "users"
  add_foreign_key "notes", "cat_events"
  add_foreign_key "notes", "clinics"
  add_foreign_key "notes", "trapping_projects"
  add_foreign_key "roles", "colonies"
  add_foreign_key "roles", "people"
  add_foreign_key "trapping_projects", "colonies"
  add_foreign_key "trapping_projects", "users"
end

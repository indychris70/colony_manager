# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

user = User.find_or_initialize_by(email: 'admin@admin.com')
user.name = 'Default Admin Account'
user.password = 'admin1'
user.password_confirmation = 'admin1'
user.admin = true
user.created_by = "seed_file"
user.save! 

dcr = DeletedCatReason.find_or_initialize_by(reason: "N/A")
dcr.description = "Not Available"
dcr.is_active = true
dcr.created_by = "seed_file"
dcr.save!

colonies = [
    { "first_name" => "Sheev", "last_name" => "Palpatine", "address1" => "5901 Blackley Lane", "city" => "Indianapolis", "state" => "IN", "zip" => "46254", "num_cats" => 2, "phone" => "317-154-9266", "primary_phone_description" => "home", "status_code_id" => 1 },
    { "first_name" => "Jabba", "last_name" => "The Hutt", "address1" => "6611 Latona Drive", "city" => "Indianapolis", "state" => "IN", "zip" => "46278", "num_cats" => 4, "phone" => "317-747-9266", "primary_phone_description" => "home", "status_code_id" => 1 },
    { "first_name" => "See", "last_name" => "Threepio", "address1" => "3511 Waterstone Court", "city" => "Indianapolis", "state" => "IN", "zip" => "46268", "num_cats" => 1, "phone" => "317-893-9685", "primary_phone_description" => "cell", "status_code_id" => 1 },
    { "first_name" => "Mon", "last_name" => "Mothma", "address1" => "7808 Marquis Lane", "city" => "Indianapolis", "state" => "IN", "zip" => "46260", "num_cats" => 6, "phone" => "317-893-9266", "primary_phone_description" => "home", "status_code_id" => 1 },
    { "first_name" => "Artoo", "last_name" => "Deetoo", "address1" => "1726 Park North Way", "city" => "Indianapolis", "state" => "IN", "zip" => "46260", "num_cats" => 12, "phone" => "317-456-8574", "primary_phone_description" => "home", "status_code_id" => 1 },
    { "first_name" => "Luke", "last_name" => "Skywalker", "address1" => "7761 Delbrook Drive", "city" => "Indianapolis", "state" => "IN", "zip" => "46260", "num_cats" => 3, "phone" => "317-642-9588", "primary_phone_description" => "work", "status_code_id" => 1 },
    { "first_name" => "Leia", "last_name" => "Organa", "address1" => "1002 Timberlane Drive", "city" => "Indianapolis", "state" => "IN", "zip" => "46260", "num_cats" => 4, "phone" => "317-857-6555", "primary_phone_description" => "home", "status_code_id" => 1 },    
    { "first_name" => "Han", "last_name" => "Solo", "address1" => "535 West 73rd Street", "city" => "Indianapolis", "state" => "IN", "zip" => "46260", "num_cats" => 5, "phone" => "317-555-9266", "primary_phone_description" => "home", "status_code_id" => 1 },
    { "first_name" => "Boba", "last_name" => "Fett", "address1" => "1328 Shawnee Road", "city" => "Indianapolis", "state" => "IN", "zip" => "46260", "num_cats" => 21, "phone" => "317-873-9261", "primary_phone_description" => "home", "status_code_id" => 1 },
    { "first_name" => "Owen", "last_name" => "Lars", "address1" => "6459 Horizon Court", "city" => "Indianapolis", "state" => "IN", "zip" => "46260", "num_cats" => 2, "phone" => "317-893-9266", "primary_phone_description" => "work", "status_code_id" => 1 },
    { "first_name" => "Darth", "last_name" => "Vader", "address1" => "2260 West Coil Street", "city" => "Indianapolis", "state" => "IN", "zip" => "46260", "num_cats" => 1, "phone" => "317-123-9266", "primary_phone_description" => "cell", "status_code_id" => 1 },
    { "first_name" => "Wedge", "last_name" => "Antilles", "address1" => "3307 Loftus Court", "city" => "Indianapolis", "state" => "IN", "zip" => "46268", "num_cats" => 7, "phone" => "317-893-4761", "primary_phone_description" => "cell", "status_code_id" => 1 },
    { "first_name" => "Grand Moff", "last_name" => "Tarkin", "address1" => "6114 Peregrine Boulevard", "city" => "Indianapolis", "state" => "IN", "zip" => "46228", "num_cats" => 2, "phone" => "317-893-9888", "primary_phone_description" => "home", "status_code_id" => 1 },
    { "first_name" => "Chewbacca", "last_name" => "", "address1" => "3203 West 57th Street", "city" => "Indianapolis", "state" => "IN", "zip" => "46228", "num_cats" => 2, "phone" => "317-893-9266", "primary_phone_description" => "home", "status_code_id" => 1 },
    { "first_name" => "Ben", "last_name" => "Kenobi", "address1" => "5531 Woodside Drive", "city" => "Indianapolis", "state" => "IN", "zip" => "46228", "num_cats" => 5, "phone" => "317-451-9266", "primary_phone_description" => "home", "status_code_id" => 1 },
    { "first_name" => "Lando", "last_name" => "Calrissian", "address1" => "6326 Hazelwood Avenue", "city" => "Indianapolis", "state" => "IN", "zip" => "46260", "num_cats" => 3, "phone" => "317-893-8522", "primary_phone_description" => "work", "status_code_id" => 1 },
    { "first_name" => "Beru", "last_name" => "Lars", "address1" => "1753 Horizon Lane", "city" => "Indianapolis", "state" => "IN", "zip" => "46260", "num_cats" => 9, "phone" => "317-555-9588", "primary_phone_description" => "home", "status_code_id" => 1 },
    { "first_name" => "Jek Tono", "last_name" => "Porkins", "address1" => "2217 Riviera Street", "city" => "Indianapolis", "state" => "IN", "zip" => "46260", "num_cats" => 3, "phone" => "317-123-5247", "primary_phone_description" => "work", "status_code_id" => 1 },
    { "first_name" => "Gial", "last_name" => "Ackbar", "address1" => "2938 Driving Wind Way", "city" => "Indianapolis", "state" => "IN", "zip" => "46268", "num_cats" => 1, "phone" => "317-987-9266", "primary_phone_description" => "cell", "status_code_id" => 1 },
    { "first_name" => "Greedo", "last_name" => "", "address1" => "1052 Loughery Lane", "city" => "Indianapolis", "state" => "IN", "zip" => "46228", "num_cats" => 25, "phone" => "317-893-1695", "primary_phone_description" => "home", "status_code_id" => 1 },
    ]
    
# colonies.each do |colony|
#     col = Colony.find_or_initialize_by(address1: colony['address1'])
#     col.first_name = colony['first_name']
#     col.last_name = colony['last_name']
#     col.address1 = colony['address1']
#     col.city = colony['city']
#     col.state = colony['state']
#     col.zip_code = colony['zip']
#     col.num_cats = colony['num_cats']
#     col.phone = colony['phone']
#     col.primary_phone_description = colony['primary_phone_description']
#     col.status_code_id = colony['status_code_id']
#     col.ins_date = Time.now
#     col.status_code_change_date = Time.now
#     col.save!
# end

# Add system ColonyPopulationEventTypes
colony_population_event_types = [
    {
        name: "Trapping Project",
        description: "A scheduled event to trap cats at a colony.",
        is_active: true,
        is_deleted: false,
        created_by: "system",
        is_system_type: true
    }
]

colony_population_event_types.each do |e|
    ColonyPopulationEventType.find_or_create_by({name: e['name'], is_system_type: e['is_system_type']}).update!(e)
end

# Add system CatEventTypes
cat_event_types = [
    { type_name: "Create", type_description: "Cat record added", is_active: true, created_by: "system", is_system_type: true, is_deleted: false },
    { type_name: "Update", type_description: "Cat information was updated", is_active: true, created_by: "system", is_system_type: true, is_deleted: false },
    { type_name: "Trapping Project", type_description: "A cat trapped as part of a trapping project", is_active: true, created_by: "system", is_system_type: true, is_deleted: false }
]

cat_event_types.each do |e|
    CatEventType.find_or_create_by({type_name: e['name'], is_system_type: e['is_system_type']}).update!(e)
end

# Add system CatEventOutcomes:
cat_event_outcomes = [
    { name: "Joined Colony", description: "A cat was added to the colony", is_active: true, created_by: "system", is_system_outcome: true, remove_cat: false, is_deleted: false },
    { name: "Update: Altered", description: "A cat record was manually updated from not altered to altered", is_active: true, created_by: "system", is_system_outcome: true, remove_cat: false, is_deleted: false },
    { name: "Update: Not Altered", description: "A cat record was manually updated from altered to not altered", is_active: true, created_by: "system", is_system_outcome: true, remove_cat: false, is_deleted: false },
]

cat_event_outcomes.each do |e|
    CatEventOutcome.find_or_create_by({name: e['name'], is_system_outcome: e['is_system_outcome']}).update!(e)
end

# Add system Role Values:
role_values = [
    { name: "Receive Updates", description: "Can be selected to receive email notifictions", is_active: true, created_by: "system", is_system_type: true }
]

role_values.each do |r|
    RoleValue.find_or_create_by({ name: r['name'], is_system_type: r['is_system_type'] }).update!(r)
end

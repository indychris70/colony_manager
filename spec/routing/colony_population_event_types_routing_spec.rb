require "rails_helper"

RSpec.describe ColonyPopulationEventTypesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/colony_population_event_types").to route_to("colony_population_event_types#index")
    end

    it "routes to #new" do
      expect(get: "/colony_population_event_types/new").to route_to("colony_population_event_types#new")
    end

    it "routes to #show" do
      expect(get: "/colony_population_event_types/1").to route_to("colony_population_event_types#show", id: "1")
    end

    it "routes to #edit" do
      expect(get: "/colony_population_event_types/1/edit").to route_to("colony_population_event_types#edit", id: "1")
    end


    it "routes to #create" do
      expect(post: "/colony_population_event_types").to route_to("colony_population_event_types#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/colony_population_event_types/1").to route_to("colony_population_event_types#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/colony_population_event_types/1").to route_to("colony_population_event_types#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/colony_population_event_types/1").to route_to("colony_population_event_types#destroy", id: "1")
    end
  end
end

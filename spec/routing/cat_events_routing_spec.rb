require "rails_helper"

RSpec.describe CatEventsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/cat_events").to route_to("cat_events#index")
    end

    it "routes to #new" do
      expect(get: "/cat_events/new").to route_to("cat_events#new")
    end

    it "routes to #show" do
      expect(get: "/cat_events/1").to route_to("cat_events#show", id: "1")
    end

    it "routes to #edit" do
      expect(get: "/cat_events/1/edit").to route_to("cat_events#edit", id: "1")
    end


    it "routes to #create" do
      expect(post: "/cat_events").to route_to("cat_events#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/cat_events/1").to route_to("cat_events#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/cat_events/1").to route_to("cat_events#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/cat_events/1").to route_to("cat_events#destroy", id: "1")
    end
  end
end

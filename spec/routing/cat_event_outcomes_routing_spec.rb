require "rails_helper"

RSpec.describe CatEventOutcomesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/cat_event_outcomes").to route_to("cat_event_outcomes#index")
    end

    it "routes to #new" do
      expect(get: "/cat_event_outcomes/new").to route_to("cat_event_outcomes#new")
    end

    it "routes to #show" do
      expect(get: "/cat_event_outcomes/1").to route_to("cat_event_outcomes#show", id: "1")
    end

    it "routes to #edit" do
      expect(get: "/cat_event_outcomes/1/edit").to route_to("cat_event_outcomes#edit", id: "1")
    end


    it "routes to #create" do
      expect(post: "/cat_event_outcomes").to route_to("cat_event_outcomes#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/cat_event_outcomes/1").to route_to("cat_event_outcomes#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/cat_event_outcomes/1").to route_to("cat_event_outcomes#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/cat_event_outcomes/1").to route_to("cat_event_outcomes#destroy", id: "1")
    end
  end
end

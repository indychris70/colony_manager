require "rails_helper"

RSpec.describe CatEventTypesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/cat_event_types").to route_to("cat_event_types#index")
    end

    it "routes to #new" do
      expect(get: "/cat_event_types/new").to route_to("cat_event_types#new")
    end

    it "routes to #show" do
      expect(get: "/cat_event_types/1").to route_to("cat_event_types#show", id: "1")
    end

    it "routes to #edit" do
      expect(get: "/cat_event_types/1/edit").to route_to("cat_event_types#edit", id: "1")
    end


    it "routes to #create" do
      expect(post: "/cat_event_types").to route_to("cat_event_types#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/cat_event_types/1").to route_to("cat_event_types#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/cat_event_types/1").to route_to("cat_event_types#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/cat_event_types/1").to route_to("cat_event_types#destroy", id: "1")
    end
  end
end

require "rails_helper"

RSpec.describe ColonyPopEventsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/colony_pop_events").to route_to("colony_pop_events#index")
    end

    it "routes to #new" do
      expect(get: "/colony_pop_events/new").to route_to("colony_pop_events#new")
    end

    it "routes to #show" do
      expect(get: "/colony_pop_events/1").to route_to("colony_pop_events#show", id: "1")
    end

    it "routes to #edit" do
      expect(get: "/colony_pop_events/1/edit").to route_to("colony_pop_events#edit", id: "1")
    end


    it "routes to #create" do
      expect(post: "/colony_pop_events").to route_to("colony_pop_events#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/colony_pop_events/1").to route_to("colony_pop_events#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/colony_pop_events/1").to route_to("colony_pop_events#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/colony_pop_events/1").to route_to("colony_pop_events#destroy", id: "1")
    end
  end
end

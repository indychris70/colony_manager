require "rails_helper"

RSpec.describe TrappingProjectsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(get: "/trapping_projects").to route_to("trapping_projects#index")
    end

    it "routes to #new" do
      expect(get: "/trapping_projects/new").to route_to("trapping_projects#new")
    end

    it "routes to #show" do
      expect(get: "/trapping_projects/1").to route_to("trapping_projects#show", id: "1")
    end

    it "routes to #edit" do
      expect(get: "/trapping_projects/1/edit").to route_to("trapping_projects#edit", id: "1")
    end


    it "routes to #create" do
      expect(post: "/trapping_projects").to route_to("trapping_projects#create")
    end

    it "routes to #update via PUT" do
      expect(put: "/trapping_projects/1").to route_to("trapping_projects#update", id: "1")
    end

    it "routes to #update via PATCH" do
      expect(patch: "/trapping_projects/1").to route_to("trapping_projects#update", id: "1")
    end

    it "routes to #destroy" do
      expect(delete: "/trapping_projects/1").to route_to("trapping_projects#destroy", id: "1")
    end
  end
end

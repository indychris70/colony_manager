require 'rails_helper'

RSpec.describe "cat_event_outcomes/new", type: :view do
  before(:each) do
    assign(:cat_event_outcome, CatEventOutcome.new(
      name: "MyString",
      description: "MyString",
      is_active: false,
      is_deleted: false,
      created_by: 1,
      deleted_by: 1,
      is_system_outcome: false
    ))
  end

  it "renders new cat_event_outcome form" do
    render

    assert_select "form[action=?][method=?]", cat_event_outcomes_path, "post" do

      assert_select "input[name=?]", "cat_event_outcome[name]"

      assert_select "input[name=?]", "cat_event_outcome[description]"

      assert_select "input[name=?]", "cat_event_outcome[is_active]"

      assert_select "input[name=?]", "cat_event_outcome[is_deleted]"

      assert_select "input[name=?]", "cat_event_outcome[created_by]"

      assert_select "input[name=?]", "cat_event_outcome[deleted_by]"

      assert_select "input[name=?]", "cat_event_outcome[is_system_outcome]"
    end
  end
end

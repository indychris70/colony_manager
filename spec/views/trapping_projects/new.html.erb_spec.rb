require 'rails_helper'

RSpec.describe "trapping_projects/new", type: :view do
  before(:each) do
    assign(:trapping_project, TrappingProject.new(
      planned_cats: 1,
      actual_cats: 1,
      assigned_user: "MyString",
      user: nil,
      colony: nil
    ))
  end

  it "renders new trapping_project form" do
    render

    assert_select "form[action=?][method=?]", trapping_projects_path, "post" do

      assert_select "input[name=?]", "trapping_project[planned_cats]"

      assert_select "input[name=?]", "trapping_project[actual_cats]"

      assert_select "input[name=?]", "trapping_project[assigned_user]"

      assert_select "input[name=?]", "trapping_project[user_id]"

      assert_select "input[name=?]", "trapping_project[colony_id]"
    end
  end
end

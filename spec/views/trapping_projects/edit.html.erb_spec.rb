require 'rails_helper'

RSpec.describe "trapping_projects/edit", type: :view do
  before(:each) do
    @trapping_project = assign(:trapping_project, TrappingProject.create!(
      planned_cats: 1,
      actual_cats: 1,
      assigned_user: "MyString",
      user: nil,
      colony: nil
    ))
  end

  it "renders the edit trapping_project form" do
    render

    assert_select "form[action=?][method=?]", trapping_project_path(@trapping_project), "post" do

      assert_select "input[name=?]", "trapping_project[planned_cats]"

      assert_select "input[name=?]", "trapping_project[actual_cats]"

      assert_select "input[name=?]", "trapping_project[assigned_user]"

      assert_select "input[name=?]", "trapping_project[user_id]"

      assert_select "input[name=?]", "trapping_project[colony_id]"
    end
  end
end

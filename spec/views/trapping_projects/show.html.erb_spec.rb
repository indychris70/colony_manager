require 'rails_helper'

RSpec.describe "trapping_projects/show", type: :view do
  before(:each) do
    @trapping_project = assign(:trapping_project, TrappingProject.create!(
      planned_cats: 2,
      actual_cats: 3,
      assigned_user: "Assigned User",
      user: nil,
      colony: nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/Assigned User/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end

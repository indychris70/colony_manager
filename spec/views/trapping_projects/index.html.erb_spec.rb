require 'rails_helper'

RSpec.describe "trapping_projects/index", type: :view do
  before(:each) do
    assign(:trapping_projects, [
      TrappingProject.create!(
        planned_cats: 2,
        actual_cats: 3,
        assigned_user: "Assigned User",
        user: nil,
        colony: nil
      ),
      TrappingProject.create!(
        planned_cats: 2,
        actual_cats: 3,
        assigned_user: "Assigned User",
        user: nil,
        colony: nil
      )
    ])
  end

  it "renders a list of trapping_projects" do
    render
    assert_select "tr>td", text: 2.to_s, count: 2
    assert_select "tr>td", text: 3.to_s, count: 2
    assert_select "tr>td", text: "Assigned User".to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
  end
end

require 'rails_helper'

RSpec.describe "clinics/index", type: :view do
  before(:each) do
    assign(:clinics, [
      Clinic.create!(
        name: "Name",
        contact1: "Contact1",
        contact2: "Contact2",
        phone1: "Phone1",
        phone2: "Phone2",
        email1: "Email1",
        email2: "Email2",
        active: false,
        deleted: false,
        deleted_at: false,
        deleted_by: "Deleted By",
        address1: "Address1",
        address2: "Address2",
        city: "City",
        state: "State",
        zip_code: "Zip Code",
        description: "MyText"
      ),
      Clinic.create!(
        name: "Name",
        contact1: "Contact1",
        contact2: "Contact2",
        phone1: "Phone1",
        phone2: "Phone2",
        email1: "Email1",
        email2: "Email2",
        active: false,
        deleted: false,
        deleted_at: false,
        deleted_by: "Deleted By",
        address1: "Address1",
        address2: "Address2",
        city: "City",
        state: "State",
        zip_code: "Zip Code",
        description: "MyText"
      )
    ])
  end

  it "renders a list of clinics" do
    render
    assert_select "tr>td", text: "Name".to_s, count: 2
    assert_select "tr>td", text: "Contact1".to_s, count: 2
    assert_select "tr>td", text: "Contact2".to_s, count: 2
    assert_select "tr>td", text: "Phone1".to_s, count: 2
    assert_select "tr>td", text: "Phone2".to_s, count: 2
    assert_select "tr>td", text: "Email1".to_s, count: 2
    assert_select "tr>td", text: "Email2".to_s, count: 2
    assert_select "tr>td", text: false.to_s, count: 2
    assert_select "tr>td", text: false.to_s, count: 2
    assert_select "tr>td", text: false.to_s, count: 2
    assert_select "tr>td", text: "Deleted By".to_s, count: 2
    assert_select "tr>td", text: "Address1".to_s, count: 2
    assert_select "tr>td", text: "Address2".to_s, count: 2
    assert_select "tr>td", text: "City".to_s, count: 2
    assert_select "tr>td", text: "State".to_s, count: 2
    assert_select "tr>td", text: "Zip Code".to_s, count: 2
    assert_select "tr>td", text: "MyText".to_s, count: 2
  end
end

require 'rails_helper'

RSpec.describe "clinics/show", type: :view do
  before(:each) do
    @clinic = assign(:clinic, Clinic.create!(
      name: "Name",
      contact1: "Contact1",
      contact2: "Contact2",
      phone1: "Phone1",
      phone2: "Phone2",
      email1: "Email1",
      email2: "Email2",
      active: false,
      deleted: false,
      deleted_at: false,
      deleted_by: "Deleted By",
      address1: "Address1",
      address2: "Address2",
      city: "City",
      state: "State",
      zip_code: "Zip Code",
      description: "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Contact1/)
    expect(rendered).to match(/Contact2/)
    expect(rendered).to match(/Phone1/)
    expect(rendered).to match(/Phone2/)
    expect(rendered).to match(/Email1/)
    expect(rendered).to match(/Email2/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(/Deleted By/)
    expect(rendered).to match(/Address1/)
    expect(rendered).to match(/Address2/)
    expect(rendered).to match(/City/)
    expect(rendered).to match(/State/)
    expect(rendered).to match(/Zip Code/)
    expect(rendered).to match(/MyText/)
  end
end

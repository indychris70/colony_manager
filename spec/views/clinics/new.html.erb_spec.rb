require 'rails_helper'

RSpec.describe "clinics/new", type: :view do
  before(:each) do
    assign(:clinic, Clinic.new(
      name: "MyString",
      contact1: "MyString",
      contact2: "MyString",
      phone1: "MyString",
      phone2: "MyString",
      email1: "MyString",
      email2: "MyString",
      active: false,
      deleted: false,
      deleted_at: false,
      deleted_by: "MyString",
      address1: "MyString",
      address2: "MyString",
      city: "MyString",
      state: "MyString",
      zip_code: "MyString",
      description: "MyText"
    ))
  end

  it "renders new clinic form" do
    render

    assert_select "form[action=?][method=?]", clinics_path, "post" do

      assert_select "input[name=?]", "clinic[name]"

      assert_select "input[name=?]", "clinic[contact1]"

      assert_select "input[name=?]", "clinic[contact2]"

      assert_select "input[name=?]", "clinic[phone1]"

      assert_select "input[name=?]", "clinic[phone2]"

      assert_select "input[name=?]", "clinic[email1]"

      assert_select "input[name=?]", "clinic[email2]"

      assert_select "input[name=?]", "clinic[active]"

      assert_select "input[name=?]", "clinic[deleted]"

      assert_select "input[name=?]", "clinic[deleted_at]"

      assert_select "input[name=?]", "clinic[deleted_by]"

      assert_select "input[name=?]", "clinic[address1]"

      assert_select "input[name=?]", "clinic[address2]"

      assert_select "input[name=?]", "clinic[city]"

      assert_select "input[name=?]", "clinic[state]"

      assert_select "input[name=?]", "clinic[zip_code]"

      assert_select "textarea[name=?]", "clinic[description]"
    end
  end
end

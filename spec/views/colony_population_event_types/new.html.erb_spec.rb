require 'rails_helper'

RSpec.describe "colony_population_event_types/new", type: :view do
  before(:each) do
    assign(:colony_population_event_type, ColonyPopulationEventType.new(
      name: "MyString",
      description: "MyString",
      is_active: false,
      is_deleted: false,
      created_by: 1,
      deleted_by: 1,
      is_system_type: false
    ))
  end

  it "renders new colony_population_event_type form" do
    render

    assert_select "form[action=?][method=?]", colony_population_event_types_path, "post" do

      assert_select "input[name=?]", "colony_population_event_type[name]"

      assert_select "input[name=?]", "colony_population_event_type[description]"

      assert_select "input[name=?]", "colony_population_event_type[is_active]"

      assert_select "input[name=?]", "colony_population_event_type[is_deleted]"

      assert_select "input[name=?]", "colony_population_event_type[created_by]"

      assert_select "input[name=?]", "colony_population_event_type[deleted_by]"

      assert_select "input[name=?]", "colony_population_event_type[is_system_type]"
    end
  end
end

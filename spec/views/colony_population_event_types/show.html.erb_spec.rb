require 'rails_helper'

RSpec.describe "colony_population_event_types/show", type: :view do
  before(:each) do
    @colony_population_event_type = assign(:colony_population_event_type, ColonyPopulationEventType.create!(
      name: "Name",
      description: "Description",
      is_active: false,
      is_deleted: false,
      created_by: 2,
      deleted_by: 3,
      is_system_type: false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Description/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/false/)
  end
end

require 'rails_helper'

RSpec.describe "colony_population_event_types/index", type: :view do
  before(:each) do
    assign(:colony_population_event_types, [
      ColonyPopulationEventType.create!(
        name: "Name",
        description: "Description",
        is_active: false,
        is_deleted: false,
        created_by: 2,
        deleted_by: 3,
        is_system_type: false
      ),
      ColonyPopulationEventType.create!(
        name: "Name",
        description: "Description",
        is_active: false,
        is_deleted: false,
        created_by: 2,
        deleted_by: 3,
        is_system_type: false
      )
    ])
  end

  it "renders a list of colony_population_event_types" do
    render
    assert_select "tr>td", text: "Name".to_s, count: 2
    assert_select "tr>td", text: "Description".to_s, count: 2
    assert_select "tr>td", text: false.to_s, count: 2
    assert_select "tr>td", text: false.to_s, count: 2
    assert_select "tr>td", text: 2.to_s, count: 2
    assert_select "tr>td", text: 3.to_s, count: 2
    assert_select "tr>td", text: false.to_s, count: 2
  end
end

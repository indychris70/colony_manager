require 'rails_helper'

RSpec.describe "cat_event_types/new", type: :view do
  before(:each) do
    assign(:cat_event_type, CatEventType.new(
      type_name: "MyString",
      type_description: "MyString",
      is_active: false,
      is_deleted: false,
      created_by: 1,
      deleted_by: 1,
      is_system_type: false
    ))
  end

  it "renders new cat_event_type form" do
    render

    assert_select "form[action=?][method=?]", cat_event_types_path, "post" do

      assert_select "input[name=?]", "cat_event_type[type_name]"

      assert_select "input[name=?]", "cat_event_type[type_description]"

      assert_select "input[name=?]", "cat_event_type[is_active]"

      assert_select "input[name=?]", "cat_event_type[is_deleted]"

      assert_select "input[name=?]", "cat_event_type[created_by]"

      assert_select "input[name=?]", "cat_event_type[deleted_by]"

      assert_select "input[name=?]", "cat_event_type[is_system_type]"
    end
  end
end

require 'rails_helper'

RSpec.describe "cat_event_types/index", type: :view do
  before(:each) do
    assign(:cat_event_types, [
      CatEventType.create!(
        type_name: "Type Name",
        type_description: "Type Description",
        is_active: false,
        is_deleted: false,
        created_by: 2,
        deleted_by: 3,
        is_system_type: false
      ),
      CatEventType.create!(
        type_name: "Type Name",
        type_description: "Type Description",
        is_active: false,
        is_deleted: false,
        created_by: 2,
        deleted_by: 3,
        is_system_type: false
      )
    ])
  end

  it "renders a list of cat_event_types" do
    render
    assert_select "tr>td", text: "Type Name".to_s, count: 2
    assert_select "tr>td", text: "Type Description".to_s, count: 2
    assert_select "tr>td", text: false.to_s, count: 2
    assert_select "tr>td", text: false.to_s, count: 2
    assert_select "tr>td", text: 2.to_s, count: 2
    assert_select "tr>td", text: 3.to_s, count: 2
    assert_select "tr>td", text: false.to_s, count: 2
  end
end

require 'rails_helper'

RSpec.describe "cat_event_types/show", type: :view do
  before(:each) do
    @cat_event_type = assign(:cat_event_type, CatEventType.create!(
      type_name: "Type Name",
      type_description: "Type Description",
      is_active: false,
      is_deleted: false,
      created_by: 2,
      deleted_by: 3,
      is_system_type: false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Type Name/)
    expect(rendered).to match(/Type Description/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/false/)
  end
end

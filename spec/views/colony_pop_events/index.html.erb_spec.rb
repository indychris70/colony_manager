require 'rails_helper'

RSpec.describe "colony_pop_events/index", type: :view do
  before(:each) do
    assign(:colony_pop_events, [
      ColonyPopEvent.create!(
        event_inflow: 2,
        event_outflow: 3,
        total_inflow: 4,
        total_outflow: 5,
        altered_population: 6,
        colony: nil,
        trapping_project: nil,
        colony_population_event_type: nil
      ),
      ColonyPopEvent.create!(
        event_inflow: 2,
        event_outflow: 3,
        total_inflow: 4,
        total_outflow: 5,
        altered_population: 6,
        colony: nil,
        trapping_project: nil,
        colony_population_event_type: nil
      )
    ])
  end

  it "renders a list of colony_pop_events" do
    render
    assert_select "tr>td", text: 2.to_s, count: 2
    assert_select "tr>td", text: 3.to_s, count: 2
    assert_select "tr>td", text: 4.to_s, count: 2
    assert_select "tr>td", text: 5.to_s, count: 2
    assert_select "tr>td", text: 6.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
  end
end

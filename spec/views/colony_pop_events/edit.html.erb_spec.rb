require 'rails_helper'

RSpec.describe "colony_pop_events/edit", type: :view do
  before(:each) do
    @colony_pop_event = assign(:colony_pop_event, ColonyPopEvent.create!(
      event_inflow: 1,
      event_outflow: 1,
      total_inflow: 1,
      total_outflow: 1,
      altered_population: 1,
      colony: nil,
      trapping_project: nil,
      colony_population_event_type: nil
    ))
  end

  it "renders the edit colony_pop_event form" do
    render

    assert_select "form[action=?][method=?]", colony_pop_event_path(@colony_pop_event), "post" do

      assert_select "input[name=?]", "colony_pop_event[event_inflow]"

      assert_select "input[name=?]", "colony_pop_event[event_outflow]"

      assert_select "input[name=?]", "colony_pop_event[total_inflow]"

      assert_select "input[name=?]", "colony_pop_event[total_outflow]"

      assert_select "input[name=?]", "colony_pop_event[altered_population]"

      assert_select "input[name=?]", "colony_pop_event[colony_id]"

      assert_select "input[name=?]", "colony_pop_event[trapping_project_id]"

      assert_select "input[name=?]", "colony_pop_event[colony_population_event_type_id]"
    end
  end
end

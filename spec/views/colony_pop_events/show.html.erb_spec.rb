require 'rails_helper'

RSpec.describe "colony_pop_events/show", type: :view do
  before(:each) do
    @colony_pop_event = assign(:colony_pop_event, ColonyPopEvent.create!(
      event_inflow: 2,
      event_outflow: 3,
      total_inflow: 4,
      total_outflow: 5,
      altered_population: 6,
      colony: nil,
      trapping_project: nil,
      colony_population_event_type: nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/4/)
    expect(rendered).to match(/5/)
    expect(rendered).to match(/6/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end

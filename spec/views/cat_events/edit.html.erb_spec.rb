require 'rails_helper'

RSpec.describe "cat_events/edit", type: :view do
  before(:each) do
    @cat_event = assign(:cat_event, CatEvent.create!(
      trapping_project: nil,
      cat: nil,
      cat_event_type: nil,
      cat_event_outcome: nil
    ))
  end

  it "renders the edit cat_event form" do
    render

    assert_select "form[action=?][method=?]", cat_event_path(@cat_event), "post" do

      assert_select "input[name=?]", "cat_event[trapping_project_id]"

      assert_select "input[name=?]", "cat_event[cat_id]"

      assert_select "input[name=?]", "cat_event[cat_event_type_id]"

      assert_select "input[name=?]", "cat_event[cat_event_outcome_id]"
    end
  end
end

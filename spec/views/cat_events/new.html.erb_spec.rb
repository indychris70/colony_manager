require 'rails_helper'

RSpec.describe "cat_events/new", type: :view do
  before(:each) do
    assign(:cat_event, CatEvent.new(
      trapping_project: nil,
      cat: nil,
      cat_event_type: nil,
      cat_event_outcome: nil
    ))
  end

  it "renders new cat_event form" do
    render

    assert_select "form[action=?][method=?]", cat_events_path, "post" do

      assert_select "input[name=?]", "cat_event[trapping_project_id]"

      assert_select "input[name=?]", "cat_event[cat_id]"

      assert_select "input[name=?]", "cat_event[cat_event_type_id]"

      assert_select "input[name=?]", "cat_event[cat_event_outcome_id]"
    end
  end
end

require 'rails_helper'

RSpec.describe "cat_events/index", type: :view do
  before(:each) do
    assign(:cat_events, [
      CatEvent.create!(
        trapping_project: nil,
        cat: nil,
        cat_event_type: nil,
        cat_event_outcome: nil
      ),
      CatEvent.create!(
        trapping_project: nil,
        cat: nil,
        cat_event_type: nil,
        cat_event_outcome: nil
      )
    ])
  end

  it "renders a list of cat_events" do
    render
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
    assert_select "tr>td", text: nil.to_s, count: 2
  end
end

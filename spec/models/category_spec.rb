require 'rails_helper'

RSpec.describe 'Category' do
    it 'has a name' do 
        category = Category.new({'name': 'CategoryName'})
        expect(category.name).to eq('CategoryName')
    end
end
RSpec.describe 'Note' do
    it 'has text' do 
        note = Note.new({'note_text': 'This is a test note.'})
        expect(note.note_text).to eq('This is a test note.')
    end
end
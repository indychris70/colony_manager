FactoryBot.define do
  factory :person do
    first_name { "MyString" }
    last_name { "MyString" }
    address1 { "MyString" }
    string { "MyString" }
    address2 { "MyString" }
    city { "MyString" }
    state { "MyString" }
    zip_code { "MyString" }
    phone { "MyString" }
    phone_description { "MyString" }
    email { "MyString" }
    email_description { "MyString" }
    alt1_phone { "MyString" }
    alt1_phone_desc { "MyString" }
    alt1_email { "MyString" }
    alt1_email_desc { "MyString" }
    alt2_phone { "MyString" }
    alt2_phone_desc { "MyString" }
    delete { false }
    deleted_at { "2021-07-17 13:23:35" }
    latitude { 1.5 }
    longitude { 1.5 }
  end
end

FactoryBot.define do
  factory :colony_population_event_type do
    name { "MyString" }
    description { "MyString" }
    is_active { false }
    deleted_at { "2022-01-05 09:50:39" }
    is_deleted { false }
    created_by { 1 }
    deleted_by { 1 }
    is_system_type { false }
  end
end

FactoryBot.define do
  factory :cat_event_type do
    type_name { "MyString" }
    type_description { "MyString" }
    is_active { false }
    deleted_at { "2021-09-23 09:20:25" }
    is_deleted { false }
    created_by { 1 }
    deleted_by { 1 }
    is_system_type { false }
  end
end

FactoryBot.define do
  factory :role_value do
    name { "MyString" }
    description { "MyString" }
    is_active { false }
    created_by { "MyString" }
    deleted_by { "MyString" }
    deleted_at { "2021-07-18 07:20:29" }
    deleted { false }
  end
end

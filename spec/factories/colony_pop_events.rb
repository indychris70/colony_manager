FactoryBot.define do
  factory :colony_pop_event do
    event_inflow { 1 }
    event_outflow { 1 }
    total_inflow { 1 }
    total_outflow { 1 }
    altered_population { 1 }
    colony { nil }
    trapping_project { nil }
    colony_population_event_type { nil }
  end
end

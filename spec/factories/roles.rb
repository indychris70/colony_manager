FactoryBot.define do
  factory :role do
    name { "MyString" }
    colony { nil }
    person { nil }
  end
end

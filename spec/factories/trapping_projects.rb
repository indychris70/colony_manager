FactoryBot.define do
  factory :trapping_project do
    scheduled_date { "2021-09-23" }
    actual_date { "2021-09-23" }
    planned_cats { 1 }
    actual_cats { 1 }
    assigned_user { "MyString" }
    user { nil }
    colony { nil }
  end
end

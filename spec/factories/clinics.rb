FactoryBot.define do
  factory :clinic do
    name { "MyString" }
    contact1 { "MyString" }
    contact2 { "MyString" }
    phone1 { "MyString" }
    phone2 { "MyString" }
    email1 { "MyString" }
    email2 { "MyString" }
    active { false }
    deleted { false }
    deleted_at { false }
    deleted_by { "MyString" }
    address1 { "MyString" }
    address2 { "MyString" }
    city { "MyString" }
    state { "MyString" }
    zip_code { "MyString" }
    description { "MyText" }
  end
end

FactoryBot.define do
  factory :cat_event do
    trapping_project { nil }
    cat { nil }
    cat_event_type { nil }
    cat_event_outcome { nil }
    start_date { "2021-09-23" }
    end_date { "2021-09-23" }
  end
end

FactoryBot.define do
  factory :cat_event_outcome do
    name { "MyString" }
    description { "MyString" }
    is_active { false }
    deleted_at { "2021-09-23 09:20:48" }
    is_deleted { false }
    created_by { 1 }
    deleted_by { 1 }
    is_system_outcome { false }
  end
end

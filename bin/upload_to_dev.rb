require 'CSV'
# fname	lname	legacy_colony_id	ff_status	will_pay	
# will_recover	will_transport	will_return	need_reloc	notes	
# history	address2	cc_assigned	cat_captain

# values for development
lduu_user_id = 15
ff_apprd = 4
ff_pay = 6
will_pay = 1
will_recover = 7
will_transport = 2
will_return = 8
need_reloc = 3

CSV.foreach("test_for_script.csv") do |row|
	legacy_colony_id = row[2]
	# puts legacy_colony_id
	colony = Colony.where(:legacy_colony_id => legacy_colony_id.to_i).first
	 puts "#{colony['legacy_colony_id']}: #{colony['first_name']} #{colony['last_name']}"

	## create categories
	category_ids = []
	## ff_status
	case row[3]
	when 'Apprd' then category_ids.push(ff_apprd)
	when 'PAY' then category_ids.push(ff_pay)
	end
	## will_pay
	category_ids.push(will_pay) if row[4] && row[4].downcase == 'y'
	## will_recover
	category_ids.push(will_recover) if row[5] && row[5].downcase == 'y'
	## will_transport
	category_ids.push(will_transport) if row[6] && row[6].downcase == 'y'
	## will_return
	category_ids.push(will_return) if row[7] && row[7].downcase == 'y'
	## need_reloc
	category_ids.push(need_reloc) if row[8] && row[8].downcase == 'y'

	colony.category_ids = category_ids
	colony.save

	## create notes
	## legacy_notes 
	if row[9] && row[9].size>0
		note_text = "LEGACY NOTES:\n"
		note_text += row[9]
		colony.notes.create!(:note_text => note_text, :user_id => lduu_user_id)
	end

	## legacy_history
	if row[10] && row[10].size>0
		note_text = "LEGACY HISTORY:\n"
		note_text += row[10]
		colony.notes.create!(:note_text => note_text, :user_id => lduu_user_id)
	end

	## Address2
	if row[11] && row[11].size>0
		note_text = "LEGACY ADDRESS2 INFO:\n"
		note_text += row[11]
		colony.notes.create!(:note_text => note_text, :user_id => lduu_user_id)
	end

	## CC
	if row[12] && row[12].size>0
		note_text = "LEGACY CAT CAPTAIN:\n"
		note_text += "#{row[12]}: #{row[13]}"
		colony.notes.create!(:note_text => note_text, :user_id => lduu_user_id)
	end
end
class TrappingProjectsController < ApplicationController
  include TrappingProjectsHelper
  before_action :set_trapping_project, only: %i[ show edit update destroy begin_finalization continue_finalization complete_finalization ]
  before_action :set_update_list, only: [:edit, :new, :continue_finalization]
  before_action :authenticate_user!

  # GET /trapping_projects or /trapping_projects.json
  def index
    @trapping_projects = TrappingProject.all
    colony_ids = @trapping_projects.map(&:colony_id)
    @colonies = []
    colony_ids.each { |cid| @colonies << Colony.find(cid) }
    @hash = get_markers(@colonies)
    @status_codes = StatusCode.all

    puts "before form"
    puts @trapping_projects

    if params && params[:commit]
      unless params[:status].empty?
        @selected_status = params[:status]
        @trapping_projects = filter_by_status(@trapping_projects, @selected_status)
      end

      puts "after status form"
      puts @trapping_projects

      unless params[:colony_name_includes].empty?
        @colony_name_includes = params[:colony_name_includes]
        @trapping_projects = filter_by_colony_name(@trapping_projects, @colony_name_includes)
      end

      puts "after colony name form"
      puts @trapping_projects

      unless params[:trapper_id].empty?
        @trapper_id = params[:trapper_id]
        @trapping_projects = @trapping_projects.trapper(@trapper_id)
      end

      puts "after trapper form"
      puts @trapping_projects

      @trapping_projects = filter_by_date(@trapping_projects, params[:date_type], params[:sdate], params[:edate])

      puts "after date form"
      puts @trapping_projects
    end
  end

  # GET /trapping_projects/1 or /trapping_projects/1.json
  def show
    @notes = @trapping_project.notes
    @colony = Colony.find(@trapping_project.colony_id)
    @colony_nearbys = @colony.nearbys(3).not_deleted
    @hash_nearbys = get_markers(@colony_nearbys)
    @hash_colony = get_markers(@colony, @colony)
    @status_codes = StatusCode.all
  end

  # GET /trapping_projects/new
  def new
    assigned_trapper = nil
    trappers = User.is_assigned_volunteer
    @trapper_options = ""
    trappers.each do |trapper|
      selected = " selected" if assigned_trapper && assigned_trapper == trapper.name
      @trapper_options += "<option value='#{trapper.id}'#{selected}>#{trapper.name}: #{trapper.email}</option>"
    end

    if params && params[:colony_id]
      @colony = Colony.find(params[:colony_id])
      @trapping_project = @colony.trapping_projects.new
    else 
      @trapping_project = TrappingProject.new
    end
  end

  # GET /trapping_projects/1/edit
  def edit
    assigned_trapper = @trapping_project.user_id
    trappers = User.is_assigned_volunteer
    @trapper_options = ""
    trappers.each do |trapper|
      selected = " selected" if assigned_trapper && assigned_trapper == trapper.id
      @trapper_options += "<option value='#{trapper.id}'#{selected}>#{trapper.name}: #{trapper.email}</option>"
    end
  end

  # POST /trapping_projects or /trapping_projects.json
  def create
    @send_email = params[:suppress_email] ? false : true
    if params[:colony_id] && params[:colony_id].empty?
      @trapping_project = TrappingProject.new(trapping_project_params)
    else 
      @colony = Colony.find(params[:colony_id])
      @trapping_project = @colony.trapping_projects.new(trapping_project_params)
    end

    if params[:user_id] != "Unassigned"
      puts "*"*88
      puts "Assigning user: #{params[:user_id]}"
      assigned_user = user(params[:user_id])
      puts "Assigned User: #{assigned_user}"
      puts "Name: #{assigned_user.name}"
      puts "ID: #{assigned_user.id}"
      @trapping_project.user_id = assigned_user.id   
      @trapping_project.assigned_user = assigned_user.name
      puts "Assignment complete."
    else 
      @trapping_project.assigned_user = params[:user_id]
    end

    respond_to do |format|
      if @trapping_project.save

        # create note
        note_text = "Trapping project created for #{@trapping_project.colony.colony_name || 'Unnamed Colony'}.\n"
        if params[:note_text] && !params[:note_text].empty?
          note_text += params[:note_text]
        end
        note = @trapping_project.notes.create(note_text: note_text, colony_id: @trapping_project.colony_id, user_id: current_user.id)

        # send email: 
        header = "A trapping project has been created for a colony by #{current_user.name.empty? ? current_user.email : current_user.name}."
        if @send_email
          recips = params[:email_recipients] || []
          recips.each do |email_address|
            NoteMailer.new_note(email_address, note, @trapping_project.colony, header).deliver
          end
        end

        format.html { redirect_to @trapping_project, notice: "Trapping project was successfully created." }
        format.json { render :show, status: :created, location: @trapping_project }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @trapping_project.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /trapping_projects/1 or /trapping_projects/1.json
  def update
    @send_email = params[:suppress_email] ? false : true
    redirect_value = params[:redirect_to] == "continue_finalization" ? continue_finalization_url(@trapping_project) : @trapping_project
    if params[:user_id] && params[:user_id] != "Unassigned"
      assigned_user = user(params[:user_id])
      @trapping_project.user_id = assigned_user.id
      @trapping_project.assigned_user = assigned_user.name
    elsif params[:user_id] && params[:user_id] == "Unassigned"
      @trapping_project.user_id = nil 
      @trapping_project.assigned_user = params[:user_id] 
    end

    respond_to do |format|
      if @trapping_project.update(trapping_project_params)
        # create note
        note_text = "Trapping project updated for #{@trapping_project.colony.colony_name || 'Unnamed Colony'}.\n"
        if params[:note_text] && !params[:note_text].empty?
          note_text += params[:note_text]
        end
        note = @trapping_project.notes.create(note_text: note_text, colony_id: @trapping_project.colony_id, user_id: current_user.id)

        # send email: 
        header = "A trapping project has been updated for a colony by #{current_user.name.empty? ? current_user.email : current_user.name}."
        if @send_email
          recips = params[:email_recipients] || []
          recips.each do |email_address|
            NoteMailer.new_note(email_address, note, @trapping_project.colony, header).deliver
          end
        end

        format.html { redirect_to redirect_value, notice: "Trapping project was successfully updated." }
        format.json { render :show, status: :ok, location: @trapping_project }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @trapping_project.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /trapping_projects/1 or /trapping_projects/1.json
  def destroy
    @trapping_project.destroy
    respond_to do |format|
      format.html { redirect_to trapping_projects_url, notice: "Trapping project was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  def begin_finalization 
    puts params
    @colony = @trapping_project.colony
  end

  def continue_finalization 
    puts params
    puts "im continuin' ur finalization"
    @colony = @trapping_project.colony
    @cats = @colony.cats
    cats_to_add = 0
    current_population = @cats.not_deleted.size
    puts "current_population #{current_population}"
    if @trapping_project.actual_cats > current_population
      cats_to_add = @trapping_project.actual_cats - current_population 
      puts "cats_to_add: #{cats_to_add}"
      cats_to_add.times { @colony.add_cat }
      @cats = @colony.cats
      @colony = @trapping_project.colony
    end

    if @trapping_project.colony_pop_event
      # prevent duplicate colony_pop_events from being created if page is reloaded.
      @trapping_project.colony_pop_event.delete
    end

    total_inflow = @colony.total_inflow
    total_outflow = @colony.total_outflow
    total_altered = @colony.total_altered
    altered_population = @colony.altered_population.size

    # create new ColonyPopEvent
    @colony_population_event = @colony.colony_pop_events.create(
      :event_inflow => cats_to_add,
      :total_inflow => total_inflow, 
      :total_outflow => total_outflow, 
      :total_altered => total_altered, 
      :altered_population => altered_population,
      :event_outflow => 0,
      :colony_id => @colony.id,
      :colony_population_event_type_id => ColonyPopulationEventType.get_trapping_project_event_id,
      :user_id => @colony.user_id,
      :status_code_id => @colony.status_code_id,
      :description => COLONY_POP_EVENTS[TRAPPING_PROJECT]
    )

    @trapping_project.colony_pop_event = @colony_population_event 
    @trapping_project.save

    puts @colony_population_event
    puts "^"*88
  end

  def complete_finalization
    puts "I'm finalizing ur finalization"
    puts params 
    @send_email = params[:suppress_email] ? false : true
    cats = @trapping_project.colony.cats 
    colony = @trapping_project.colony
    colony_pop_event = @trapping_project.colony_pop_event
    trapping_project_id = @trapping_project.id 
    cat_event_type = CatEventType.find_by(type_name: "Trapping Project")
    cat_event_type_id = cat_event_type.id 

    cats_removed = colony_pop_event.total_outflow
    cats_altered = colony_pop_event.altered_population
    event_altered = 0
    event_outflow = 0
    
    cats.each do |cat| 
      id = cat.id.to_s 
      if params[id] == "1"
        # create cat event
        cat_event_outcome_id = params["#{id}_cat_event_outcome_id"]

        # determine if the cat was removed from the colony
        cat_event_outcome = CatEventOutcome.find(cat_event_outcome_id)
        if cat_event_outcome.remove_cat
          event_outflow += 1
          cat.deleted = true 
          cat.deleted_by = @trapping_project.user_id
          cat.deleted_at = Time.now 
          cat.delete_reason = cat_event_outcome.name
        end

        # determine if the cat was altered
        new_sex = params["#{id}_sex"]
        if ["male", "female", "unknown"].include?(cat.sex) && ["spayed", "neutered"].include?(new_sex)
          event_altered += 1
        end 

        if ["spayed", "neutered"].include?(cat.sex) && ["male", "female", "unknown"].include?(new_sex)
          event_altered -= 1
        end

        date = @trapping_project.actual_date
        # cat.cat_events.create(trapping_project_id: trapping_project_id, cat_event_type_id: cat_event_type_id, cat_event_outcome_id: cat_event_outcome_id, start_date: date, end_date: date)
        
        # set the clinic
        clinic_id = params["#{id}_clinic_id"]
        clinic = Clinic.find(clinic_id)

        # add the clinic to the cat event and trapping project
        @trapping_project.clinics << clinic 

        # update cat record
        cat.name = params["#{id}_name"]
        cat.id_code = params["#{id}_id_code"]
        cat.weight_lbs = params["#{id}_weight_lbs"]
        cat.weight_oz = params["#{id}_weight_oz"]
        cat.dob = params["#{id}_dob"]
        cat.dob_certainty = params["#{id}_dob_certainty"] || false
        cat.sex = new_sex
        cat.physical_description = params["#{id}_physical_description"]
        cat.medical_info = params["#{id}_medical_info"]
        cat.save!

        # create cat event
        cat_event = cat.cat_events.new 
        cat_event.trapping_project = @trapping_project 
        cat_event.cat_event_outcome = cat_event_outcome
        cat_event.cat_event_type = cat_event_type
        cat_event.clinic = clinic
        cat_event.start_date = date 
        cat_event.end_date = date
        cat_event.save!
      end 
    end

    ## udpate outflow for colony_pop_event and colony records
    colony_pop_event.event_outflow = event_outflow 
    colony_pop_event.total_outflow += event_outflow
    colony.total_outflow += event_outflow 
    colony.num_cats -= event_outflow

    ## update altered cats for colony_pop_event and colony
    colony_pop_event.event_altered = event_altered
    colony_pop_event.total_altered += event_altered 
    colony_pop_event.altered_population = colony.altered_population.size
    colony.total_altered += event_altered

    ## save updates
    colony_pop_event.save 
    colony.save 

    @trapping_project.is_finalized = true 

    if @trapping_project.save!
      if @send_email
        header = "A trapping project has been finalized by #{current_user.name.empty? ? current_user.email : current_user.name}."
        note = colony.notes.new
        note.note_text = "Final stats for trapping event:\n\n"
        note.note_text += "Trapping project date: #{@trapping_project.actual_date}\n"
        note.note_text += "Cats trapped: #{@trapping_project.actual_cats}\n"
        note.note_text += "Cats altered: #{colony_pop_event.event_altered}\n"
        note.user = current_user
        note.trapping_project = @trapping_project 
        note.save
        recips = params[:email_recipients] || []
        recips.each do |email_address|
          NoteMailer.new_note(email_address, note, colony, header).deliver
        end
      end
      redirect_to @trapping_project, notice: "Trapping Project successfully finalized."
    else
      redirect_to @trapping_project, alert: "Trapping Project was not finalized!"
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trapping_project
      @trapping_project = TrappingProject.find(params[:id])
    end

    def get_option_data(user)
      is_included = true
      if user.id == @trapping_project.colony.user_id 
        description = "Assigned volunteer"
      elsif user.admin
        description = "Admin user"
      else 
        description = "Non-admin user"
        is_included = false
      end
      return {
        name: user.name, 
        email: user.email, 
        description: description, 
        is_included: is_included
      }
    end

    def set_update_list
      update_list = []

      # Get all users
      users = User.all 
      user_update_list = users.map { |u| get_option_data(u) }

      # Get all colony update role persons
      people = @trapping_project.colony.people.has_receive_updates_role.uniq
      people_update_list = people.map { |pe| { name: pe.full_name, email: pe.email, description: "Colony team", is_included: true } }

      update_list += user_update_list
      update_list += people_update_list

      @update_options = ""
      update_list.each do |option|
        if option[:is_included]
          name = option[:name]
          email = option[:email]
          description = option[:description]
          @update_options += "<option value='#{email}' selected>#{name} (#{description})</option>"
        end
      end
    end

    # Only allow a list of trusted parameters through.
    def trapping_project_params
      params.require(:trapping_project).permit(:scheduled_date, :actual_date, :planned_cats, :actual_cats, :assigned_user, :user_id, :colony_id)
    end
end

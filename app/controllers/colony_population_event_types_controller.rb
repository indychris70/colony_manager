class ColonyPopulationEventTypesController < ApplicationController
  before_action :set_colony_population_event_type, only: %i[ show edit update destroy ]

  # GET /colony_population_event_types or /colony_population_event_types.json
  def index
    @colony_population_event_types = ColonyPopulationEventType.all
  end

  # GET /colony_population_event_types/1 or /colony_population_event_types/1.json
  def show
  end

  # GET /colony_population_event_types/new
  def new
    @colony_population_event_type = ColonyPopulationEventType.new
  end

  # GET /colony_population_event_types/1/edit
  def edit
  end

  # POST /colony_population_event_types or /colony_population_event_types.json
  def create
    @colony_population_event_type = ColonyPopulationEventType.new(colony_population_event_type_params)

    respond_to do |format|
      if @colony_population_event_type.save
        format.html { redirect_to @colony_population_event_type, notice: "Colony population event type was successfully created." }
        format.json { render :show, status: :created, location: @colony_population_event_type }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @colony_population_event_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /colony_population_event_types/1 or /colony_population_event_types/1.json
  def update
    respond_to do |format|
      if @colony_population_event_type.update(colony_population_event_type_params)
        format.html { redirect_to @colony_population_event_type, notice: "Colony population event type was successfully updated." }
        format.json { render :show, status: :ok, location: @colony_population_event_type }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @colony_population_event_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /colony_population_event_types/1 or /colony_population_event_types/1.json
  def destroy
    @colony_population_event_type.destroy
    respond_to do |format|
      format.html { redirect_to colony_population_event_types_url, notice: "Colony population event type was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_colony_population_event_type
      @colony_population_event_type = ColonyPopulationEventType.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def colony_population_event_type_params
      params.require(:colony_population_event_type).permit(:name, :description, :is_active, :deleted_at, :is_deleted, :created_by, :deleted_by, :is_system_type)
    end
end

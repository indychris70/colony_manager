class ApplicationController < ActionController::Base
  include ActionView::Helpers::TextHelper

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

private

	def set_return_to
		session[:return_to] ||= request.referer
	end

	def require_signin
		unless user_signed_in?
			session[:intended_url] = request.url
			redirect_to new_session_url, alert: "Please sign in!"
			
		end
	end

	def require_admin
		unless current_user_admin?
			if session[:admin_deleted_own_session] == true
				session[:admin_deleted_own_session] = nil 
				redirect_to root_url, notice: 'You deleted your account.'
			else
				redirect_to root_url, alert: "Not Authorized."
			end
		end
	end

	def current_user_admin?
		current_user && current_user.admin?
	end

	# def current_user
	# 	begin
	# 		# @current_user ||= User.find(session[:user_id]) if session[:user_id]	
	# 	rescue Exception => e 
	# 		# session[:user_id] = nil
	# 		# redirect_to root_url, alert: "Oops! There was a problem: #{e}"
	# 	end
	# end

	def current_user?(user)
		current_user == user
	end


	def user(id)
		User.find_by(id: id.to_i)
	end

	def colony(id)
		Colony.find_by(id: id.to_i)
	end

	def cat(id)
		Cat.find_by(id: id.to_i)
	end

	def trapping_project(id)
		TrappingProject.find_by(id: id.to_i)
	end

	def cat_event(id) 
		CatEvent.find_by(id: id.to_i)
	end

	def status_code(id=0)
		status_code = StatusCode.find_by_id(id.to_i)
		if status_code.nil?
			redirect_to map_path, alert: "Status Code not found."
			return
		else
			return status_code
		end
	end

	def get_people_markers(person_object, colony_marker_formatted_address_list)
		Gmaps4rails.build_markers(person_object) do |person, marker|
			if person.respond_to?('size') && person.size >= 1
				person = person.compact.first 
			end

			color_code = "CCCCFF" # "Periwinkle"
			num_available_roles = person.roles.available.size
			char_code = num_available_roles < 9 ? num_available_roles : "%2B"

			url = "https://chart.googleapis.com/chart?chst=d_simple_text_icon_below&chld=|12|000|glyphish_heart|16|F00|000"
			
			marker.lng person.longitude
			marker.lat person.latitude

			if colony_marker_formatted_address_list.include?(person.formatted_address)
				# url = "https://chart.googleapis.com/chart?chst=d_map_xpin_letter&chld=pin_sleft|#{char_code}|#{color_code}|000000"
				# marker.lng person.longitude - 0.001
				marker.lat person.latitude - 0.002
			else
				# url = "https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=#{char_code}|#{color_code}|000000"
				# marker.lng person.longitude
				# marker.lat person.latitude
			end
			
			marker.picture({
				:url => url,
				:width   => 21,
				:height  => 34
			})

			infowindow_html = "<strong>Person: #{person.first_name} #{person.last_name}</strong> "
			infowindow_html += "<a href='#{person_path(person)}' class='btn btn-xs btn-primary'>View</a>"
			infowindow_html += "<br>#{person.formatted_address}"
			infowindow_html += "<br><span>Available Roles: </span>"
			person.roles.available.each do |role|
				infowindow_html += "<span class='label label-default'>#{role.name}</span> "
			end

			marker.infowindow infowindow_html
		end
	end

    def get_markers(colony_object, starred_colony=nil,color_scheme=nil)
      Gmaps4rails.build_markers(colony_object) do |colony, marker|
		  if colony.respond_to?('size') && colony.size >= 1
			colony = colony.compact.first
		  end
      	case color_scheme
			when 'location_type' then 
				color_code = LOCATION_TYPES[colony.location_type][:color].gsub('#','')
			else color_code = colony.status_code.color.gsub('#','')
		end
		  
		# Determine if colony has scheduled trapping project
		trapping_projects = colony.trapping_projects.not_finalized
		has_scheduled_trapping_project = trapping_projects.size > 0

		# Add UTF-8 character code to designate a colony with certain conditions
		# codes from https://www.utf8-chartable.de/unicode-utf8-table.pl?start=8192&number=128
		if starred_colony && colony.id == starred_colony.id 
			char_code = "#{colony.num_cats < 10 ? colony.num_cats : "%2B"}%E2%80%A2" # colony show page
			url = "https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=#{char_code}|#{color_code}|000000"
		elsif has_scheduled_trapping_project
			char_code = "#{colony.num_cats < 10 ? colony.num_cats : "%2B"}"
			url = "https://chart.googleapis.com/chart?chst=d_map_xpin_letter&chld=pin_star|#{char_code}|#{color_code}|000000|FFD700"
		else 
			char_code = "#{colony.num_cats < 10 ? colony.num_cats : "%2B"}" # colony show page
			url = "https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=#{char_code}|#{color_code}|000000"
		end

		puts "*"*88
		puts url
		puts "*"*88
		
		marker.lat colony.latitude
        marker.lng colony.longitude
        marker.picture({
				     # :url => "https://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=#{char_code}|#{color_code}",
				     :url => url,
					 :width   => 21,
				     :height  => 34
  						})
        infowindow_html = "<strong>Colony: "      
        infowindow_html += colony.colony_name ? "#{colony.colony_name} " : "Unnamed Colony"
        # infowindow_html += "#{colony.last_name}" if colony.last_name && colony.last_name.size>0 
        infowindow_html += "</strong> <a href='#{colony_path(colony)}' class='btn btn-xs btn-primary'>View</a>"
        infowindow_html += "<br><strong>Business:</strong> #{colony.business_name}<br>" if colony.business_name && colony.business_name.size>0
        infowindow_html += "<br>#{colony.formatted_address}"
		infowindow_html += "<br>" 
		infowindow_html += "<span class='label label-info'>Team: #{colony.people.uniq.size}</span> "
        infowindow_html += "<span class='label label-default'>#{pluralize(colony.num_cats, 'Cat')}</span> 
        					<abbr title='#{colony.status_code.description}'><span class='label label-success' 
        					style='background-color:#{colony.status_code.color};'><a class='label-link' 
        					href='/map?params_present=true&status_code_id[]=#{colony.status_code.id}'>
        					#{colony.status_code.status_code}</a></span></abbr> <span class='label label-success'>
        					<a class='label-link' href='/map?params_present=true&user_id[]=#{colony.user_id}'>
							#{colony.user.name if colony.user}</a></span>" 
							
		infowindow_html += "&nbsp;<span class='label label-info'>
								<span id='filter-tooltip' aria-hidden='true' data-toggle='tooltip' data-placement='top' title='This colony has a scheduled trapping project. Click on the View link for details.'>
									<span class='glyphicon glyphicon-calendar'></span>
									#{colony.trapping_projects.not_finalized.minimum(:scheduled_date)}
								</span>
							</span>" if colony.trapping_projects.not_finalized.length > 0
		infowindow_html += "<br />"
        marker.infowindow infowindow_html
      end
    end

	# helper_method :current_user
	# helper_method :current_user?
	helper_method :current_user_admin?
	helper_method :user
	helper_method :status_code
end

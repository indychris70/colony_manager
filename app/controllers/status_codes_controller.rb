class StatusCodesController < ApplicationController
  # Authentication with Devise. This line must be after the `protect_from_forgery` call (in application controller).
	before_action :authenticate_user!
  before_action :set_status_code, only: [:show, :edit, :update, :destroy]
  before_action :require_admin
  before_action :set_return_to, only: [:destroy]

  # GET /status_codes
  # GET /status_codes.json
  def index
    @status_codes = StatusCode.all
  end

  # GET /status_codes/1
  # GET /status_codes/1.json
  def show
  end

  # GET /status_codes/new
  def new
    @status_code = StatusCode.new
    @color_options = get_color_options(@status_code.color)
  end

  # GET /status_codes/1/edit
  def edit
    @color_options = get_color_options(@status_code.color)
  end

  # POST /status_codes
  # POST /status_codes.json
  def create
    @status_code = StatusCode.new(status_code_params)
    @status_code.created_by = current_user.id
    @status_code.color = params[:color]

    respond_to do |format|
      if @status_code.save
        format.html { redirect_to @status_code, notice: 'Status code was successfully created.' }
        format.json { render :show, status: :created, location: @status_code }
      else
        format.html { render :new }
        format.json { render json: @status_code.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /status_codes/1
  # PATCH/PUT /status_codes/1.json
  def update
    # fail
    @status_code.color = params[:color]
    respond_to do |format|
      if @status_code.update(status_code_params)
        format.html { redirect_to @status_code, notice: 'Status code was successfully updated.' }
        format.json { render :show, status: :ok, location: @status_code }
      else
        format.html { render :edit }
        format.json { render json: @status_code.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /status_codes/1
  # DELETE /status_codes/1.json
  def destroy
    if Colony.status_code_id(@status_code).size > 0
      respond_to do |format| 
        format.html { redirect_to session.delete(:return_to), alert: 'Status code can not be deleted while there are colonies assigned to it.' }
        format.json { head :no_content }
      end
    else
      @status_code.destroy
      respond_to do |format|
        format.html { redirect_to session.delete(:return_to), notice: 'Status code was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
  end

  private

    def get_color_options(selected_color)
      colors = { "Green" => "#7bd148" , 
                  "Bold blue" => "#5484ed" , 
                  "Blue" => "#a4bdfc" , 
                  "Turquoise" => "#46d6db" , 
                  "Light green" => "#7ae7bf" , 
                  "Bold green" => "#51b749" , 
                  "Yellow" => "#fbd75b" , 
                  "Orange" => "#ffb878" , 
                  "Red" => "#ff887c" , 
                  "Bold red" => "#dc2127" , 
                  "Purple" => "#dbadff" , 
                  "Gray" => "#e1e1e1",
                  "Olive" => "#808000",
                  "Maroon" => "#800000",
                  "Dark blue" => "#00008B",
                  "Blue gray" => "#98AFC7"
                }

      color_options = ""
      colors.each do |name,hex|
        selected = " selected" if selected_color == hex
        color_options += "<option value='#{hex}'#{selected}>#{name}</option>"
      end
      color_options
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_status_code
      @status_code = StatusCode.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def status_code_params
      params.require(:status_code).permit(:status_code, :description, :is_active, :created_by, :deleted_by, :deleted_at, :color)
    end
end

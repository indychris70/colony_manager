class OldSessionsController < ApplicationController
	def new
		@default_user_exists = User.email('admin@admin.com').size>0
		@number_of_admin_users = User.is_admin.size
	end

	def create
		# if user = User.authenticate(params[:email], params[:password])
			# session[:user_id] = current_user.id
			# redirect_to(session.delete(:intended_url) || root_url , notice: "Welcome back, #{user.name}!")
		# else
		# 	flash.now[:alert] = "Invalid email/password combination!"
		# 	render :new
		# end
		redirect_to(root_url, notice: "Welcome! You are now signed in.")
	end

	def destroy
	  # session[:user_id] = nil
	  redirect_to root_url, notice: "You're now signed out!"
	end
end
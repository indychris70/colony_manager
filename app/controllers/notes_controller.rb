class NotesController < ApplicationController
	# Authentication with Devise. This line must be after the `protect_from_forgery` call (in application controller).
	before_action :authenticate_user!
	
	def create
		@send_email = params[:suppress_email] ? false : true
		@colony = Colony.find(params[:colony_id])
		
		@header = "A new note has been added to a colony."
		@note = @colony.notes.new(note_params)
		@note.user = current_user
		if @note.save
			if @send_email
				recips = params[:email_recipients] || []
				recips.each do |email_address|
					NoteMailer.new_note(email_address, @note, @colony, @header).deliver
				end
			end
		end
		if params[:cat_id]
			@cat = Cat.find(params[:cat_id])
			@number_of_notes = @cat.notes.size
		else
			@number_of_notes = @colony.notes.size
		end 
		respond_to do |format|	
			format.html { redirect_to @colony }
			format.js # render notes/create.js.erb
		end	
	end

private
	def note_params
      params.require(:note).permit(:note_text,:colony_id,:cat_id)
	end
end

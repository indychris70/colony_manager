class DeletedCatReasonsController < ApplicationController
  # Authentication with Devise. This line must be after the `protect_from_forgery` call (in application controller).
	before_action :authenticate_user!
  before_action :set_deleted_cat_reason, only: [:show, :edit, :update, :destroy]
  before_action :require_admin
  before_action :set_return_to, only: [:destroy]

  # GET /deleted_cat_reasons
  # GET /deleted_cat_reasons.json
  def index
    @deleted_cat_reasons = DeletedCatReason.all
  end

  # GET /deleted_cat_reasons/1
  # GET /deleted_cat_reasons/1.json
  def show
  end

  # GET /deleted_cat_reasons/new
  def new
    @deleted_cat_reason = DeletedCatReason.new
  end

  # GET /deleted_cat_reasons/1/edit
  def edit
  end

  # POST /deleted_cat_reasons
  # POST /deleted_cat_reasons.json
  def create
    @deleted_cat_reason = DeletedCatReason.new(deleted_cat_reason_params)
    @deleted_cat_reason.created_by = current_user.id

    respond_to do |format|
      if @deleted_cat_reason.save
        format.html { redirect_to @deleted_cat_reason, notice: 'Deleted Cat Reason was successfully created.' }
        format.json { render :show, status: :created, location: @deleted_cat_reason }
      else
        format.html { render :new }
        format.json { render json: @deleted_cat_reason.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /deleted_cat_reasons/1
  # PATCH/PUT /deleted_cat_reasons/1.json
  def update
    # fail
    @deleted_cat_reason.color = params[:color]
    respond_to do |format|
      if @deleted_cat_reason.update(deleted_cat_reason_params)
        format.html { redirect_to @deleted_cat_reason, notice: 'Deleted Cat Reason was successfully updated.' }
        format.json { render :show, status: :ok, location: @deleted_cat_reason }
      else
        format.html { render :edit }
        format.json { render json: @deleted_cat_reason.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /deleted_cat_reasons/1
  # DELETE /deleted_cat_reasons/1.json
  def destroy
    @deleted_cat_reason.destroy
    respond_to do |format|
      format.html { redirect_to session.delete(:return_to), notice: 'Deleted Cat Reason was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_deleted_cat_reason
      @deleted_cat_reason = DeletedCatReason.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def deleted_cat_reason_params
      params.require(:deleted_cat_reason).permit(:reason, :description, :is_active, :created_by)
    end
end


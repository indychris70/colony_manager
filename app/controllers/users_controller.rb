class UsersController < ApplicationController
	# Authentication with Devise. This line must be after the `protect_from_forgery` call (in application controller).
	before_action :authenticate_user!, except: [:new, :create]
	before_action :require_correct_user, only: [:edit, :update, :show]
	before_action :require_admin, only: [:index,:new,:create,:destroy]
	before_action :set_return_to, only: [:destroy]

	def index
		@users = User.all
	end

	def show
		puts "*"*88
		puts "hi"*88
		puts params
		@params_hash = params
		@user = User.find(params[:id])
		@status_codes = StatusCode.not_deleted
  		@results_message = ""
	    params.each do |param,value|
	      if [[""],""].include?(value)
	        params[param] = nil
	      else
	        @results_message += "#{param}: #{value} " unless ['utf8','action','controller'].include?(param)
	      end
	    end

	    @colonies = @user.colonies.not_deleted.filter_it(params.slice(:county, :zip_code, :city, :township, :neighborhood, 
	    												:min_cats, :max_cats, :last_name_starts_with, 
	    												:first_name_starts_with, :status_code_id,
	    												:address_contains, :business_name_contains, :colony_name_contains, :max_ins_date, :min_ins_date))

	    formatted_addresses = @colonies.pluck(:formatted_address).uniq
		
		@header = "User #{@user.name}, #{@colonies.size} Colonies Total"

		max_latitude = -90
        max_longitude = -180
        min_latitude = 90
        min_longitude = 180

        @colonies.each do |colony|
          max_latitude = colony.latitude if colony.latitude > max_latitude
          max_longitude = colony.longitude if colony.longitude > max_longitude
          min_latitude = colony.latitude if colony.latitude < min_latitude
          min_longitude = colony.longitude if colony.longitude < min_longitude 
        end

        @people = Person.with_available_roles.within_bounding_box([min_latitude - 0.01, min_longitude - 0.01], [max_latitude + 0.01, max_longitude + 0.01])

		@hash = get_people_markers(@people, formatted_addresses)
	    @hash += get_markers(@colonies)
	end

	def new
	  	@user = User.new
	  	@header = "Add a New User"
	end

	def create
		@user = User.new(user_params)
		if @user.save
			# session[:user_id] = @user.id
			redirect_to @user, notice: "User Created!"
		else
			render :new
		end
	end

	def edit
		@user = User.find(params[:id])
		@header = "Edit an Existing User"
	end

	def update
		@user = User.find(params[:id])
		if @user.update(user_params)
			redirect_to @user, notice: "Account successfully updated!"
		else
			render :edit
		end
	end

	def destroy
		@user = User.find(params[:id])
		@user.colonies.each { |c| c.user = nil; c.save }
		@user.destroy

		# session[:user_id] = nil if params[:id] == session[:id]
		session[:admin_deleted_own_session] = true
		redirect_to session.delete(:return_to), notice: "Account successfully deleted!"
	end

	def edit_invite_limit
		@user = User.find(params[:id])
		invite_limit = params[:invite_limit].to_i
		@user.invitation_limit = invite_limit
		if @user.save
		  	flash[:notice] = "User invite limit has been updated."
		else 
		  	flash[:alert] = "Change was not saved."
		end
		
		redirect_to @user
	  end

private

	def user_params
	  params.require(:user).
	    permit(:name, :email, :password, :password_confirmation, :admin, :field_volunteer)
	end

	def require_correct_user
		unless current_user_admin?
			@user = User.find(params[:id])
			redirect_to( root_path, alert: "Not Authorized." ) unless current_user?(@user)
		end
	end
end

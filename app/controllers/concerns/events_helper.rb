module EventsHelper

    def create_colony_pop_event(colony, description, event_inflow=0, event_outflow=0, event_altered=0)
        last_colony_pop_event = colony.colony_pop_events.last
        total_inflow = last_colony_pop_event ? last_colony_pop_event.total_inflow + event_inflow : event_inflow
        total_outflow = last_colony_pop_event ? last_colony_pop_event.total_outflow + event_outflow : event_outflow
        total_altered = last_colony_pop_event ? last_colony_pop_event.total_altered + event_altered : event_altered
        colony.colony_pop_events.create(
        event_inflow: event_inflow, 
        event_outflow: event_outflow,
        event_altered: event_altered,
        total_inflow: total_inflow,
        total_outflow: total_outflow,
        total_altered: total_altered,
        altered_population: colony.altered_population.size,
        status_code_id: colony.status_code_id,
        user_id: colony.user_id,
        description: description
        )
        colony.total_inflow = total_inflow
        colony.total_outflow = total_outflow
        colony.total_altered = total_altered
        colony.save
    end

    def create_cat_event(cat)
    end
    
end
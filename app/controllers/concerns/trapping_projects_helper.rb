module TrappingProjectsHelper

    def filter_by_status(trapping_projects, status)
        case status
        when "Scheduled"
          then return trapping_projects.scheduled 
        when "Finalized"
          then return  trapping_projects.finalized 
        when "Not Ready"
          then return trapping_projects.not_ready 
        else
            return trapping_projects 
        end
    end

    def filter_by_colony_name(trapping_projects, filter_term)
        puts "filter_by_colony_name"
        filtered_trapping_project_ids = []
        trapping_projects && trapping_projects.each do |tp|
            colony = tp.colony 
            puts colony
            colony_name = colony && colony.colony_name ? colony.colony_name : "Unnamed Colony"
            puts colony_name
            puts colony_name.include? filter_term
            filtered_trapping_project_ids << tp.id if colony_name.include? filter_term
        end
        puts "filtered trapping projects"
        puts filtered_trapping_project_ids
        return trapping_projects.filter_ids(filtered_trapping_project_ids)
    end

    def filter_by_date(trapping_projects, date_type, start_date, end_date)
        if date_type == "Scheduled"
            trapping_projects = trapping_projects.scheduled_on_or_after(start_date) unless start_date.empty?
            trapping_projects = trapping_projects.scheduled_on_or_before(end_date) unless end_date.empty? 
        elsif date_type == "Actual"
            trapping_projects = trapping_projects.actual_on_or_after(start_date) unless start_date.empty?
            trapping_projects = trapping_projects.actual_on_or_before(end_date) unless end_date.empty?
        end
        return trapping_projects
    end

  end
  
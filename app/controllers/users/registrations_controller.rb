# frozen_string_literal: true

class Users::RegistrationsController < Devise::RegistrationsController
  # before_action :configure_sign_up_params, only: [:create]
  # before_action :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  def new
    redirect_to root_path, alert: "Sign up not allowed. You must request an invitation to join."
  end

  # POST /resource
  def create
    redirect_to root_path, alert: "Sign up not allowed. You must request an invitation to join."
  end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  # def update
  #   super
  # end
  def update
    super
    @user = User.find(params[:id])
    name = params[:user][:name]
    email = params[:user][:email]
    admin = params[:user][:admin]
    field_volunteer = params[:user][:field_volunteer]
    if !@user.update(:name => name, :email => email, :admin => admin, :field_volunteer => field_volunteer)
      flash[:alert] = "User attributes did not get saved."
		end
	end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_up_params
  #   devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute])
  # end

  # If you have extra params to permit, append them to the sanitizer.
  def configure_account_update_params
    devise_parameter_sanitizer.permit(:account_update, keys: [:name, :email, :admin, :field_volunteer])
  end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
end

class Users::InvitationsController < Devise::InvitationsController
    before_action :require_admin, only: [:new]
    
    def update
        User.accept_invitation!(update_resource_params)
        redirect_to new_user_session_path, notice: "Your account has been successfully created! Please sign in to continue."
    end

private 
    def update_resource_params
        params.require(:user).permit(:password, :password_confirmation, :invitation_token, :name)
    end
end
class RoleValuesController < ApplicationController
    before_action :authenticate_user!
    before_action :set_role_value, only: [:show, :edit, :update, :destroy]

    def index
        @role_values = RoleValue.all 
    end

    def show
    end

    def edit 
    end

    def update
        respond_to do |format|
            if @role_value.update(role_value_params)
                format.html { redirect_to @role_value, notice: 'Role Value was successfully updated.' }
                format.json { render :show, status: :ok, location: @role_value }
            else
                format.html { render :edit }
                format.json { render json: @role_value.errors, status: :unprocessable_entity }
            end
        end
    end

    def new 
        @role_value = RoleValue.new
    end

    def create
        @role_value = RoleValue.new(role_value_params)
        @role_value.created_by = current_user.id
    
        respond_to do |format|
            if @role_value.save
                format.html { redirect_to @role_value, notice: 'Role Value was successfully created.' }
                format.json { render :show, status: :created, location: @role_value }
            else
                format.html { render :new }
                format.json { render json: @role_value.errors, status: :unprocessable_entity }
            end
        end
    end

    def destroy

        # if RoleValue.roles.size > 0
        #     respond_to do |format| 
        #         format.html { redirect_to session.delete(:return_to), alert: 'Role Value can not be deleted while in use.' }
        #         format.json { head :no_content }
        #     end
        # else
        @role_value.destroy
        respond_to do |format|
            format.html { redirect_to role_values_path, notice: 'Role Value was successfully destroyed.' }
            format.json { head :no_content }
        end
        # end
    end

private 

    def set_role_value
        @role_value = RoleValue.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def role_value_params
        params.require(:role_value).permit(:name, :description, :is_active, :created_by, :deleted_by, :deleted_at)
    end

end
class ColonyPopEventsController < ApplicationController
  before_action :set_colony_pop_event, only: %i[ show edit update destroy ]

  # GET /colony_pop_events or /colony_pop_events.json
  def index
    @colony_pop_events = ColonyPopEvent.all
  end

  # GET /colony_pop_events/1 or /colony_pop_events/1.json
  def show
  end

  # GET /colony_pop_events/new
  def new
    @colony_pop_event = ColonyPopEvent.new
  end

  # GET /colony_pop_events/1/edit
  def edit
  end

  # POST /colony_pop_events or /colony_pop_events.json
  def create
    @colony_pop_event = ColonyPopEvent.new(colony_pop_event_params)

    respond_to do |format|
      if @colony_pop_event.save
        format.html { redirect_to @colony_pop_event, notice: "Colony pop event was successfully created." }
        format.json { render :show, status: :created, location: @colony_pop_event }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @colony_pop_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /colony_pop_events/1 or /colony_pop_events/1.json
  def update
    respond_to do |format|
      if @colony_pop_event.update(colony_pop_event_params)
        format.html { redirect_to @colony_pop_event, notice: "Colony pop event was successfully updated." }
        format.json { render :show, status: :ok, location: @colony_pop_event }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @colony_pop_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /colony_pop_events/1 or /colony_pop_events/1.json
  def destroy
    @colony_pop_event.destroy
    respond_to do |format|
      format.html { redirect_to colony_pop_events_url, notice: "Colony pop event was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_colony_pop_event
      @colony_pop_event = ColonyPopEvent.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def colony_pop_event_params
      params.require(:colony_pop_event).permit(:event_inflow, :event_outflow, :event_altered, :total_inflow, :total_outflow, :total_altered, :altered_population, :colony_id, :trapping_project_id, :colony_population_event_type_id)
    end
end

class SavedFiltersController < ApplicationController
	# Authentication with Devise. This line must be after the `protect_from_forgery` call (in application controller).
	before_action :authenticate_user!
	before_action :set_return_to, only: [:destroy]
	
	def index
		@user_filters = current_user.saved_filters
	end

	def show
		# fail
		@user_filter = SavedFilter.find(params[:id])
		session[:params] = @user_filter.filter_params
		redirect_to map_path
	end
	def destroy
		@user_filter = SavedFilter.find(params[:id])
		@user_filter.destroy
		respond_to do |format|
			  format.html { redirect_to session.delete(:return_to), notice: 'Filter was successfully deleted.' }
			  format.json { head :no_content }
		end
	end
end

class CatEventsController < ApplicationController
  before_action :set_cat_event, only: %i[ show edit update destroy ]

  # GET /cat_events or /cat_events.json
  def index
    @cat_events = CatEvent.all
  end

  # GET /cat_events/1 or /cat_events/1.json
  def show
  end

  # GET /cat_events/new
  def new
    @cat_event = CatEvent.new
  end

  # GET /cat_events/1/edit
  def edit
  end

  # POST /cat_events or /cat_events.json
  def create
    @cat_event = CatEvent.new(cat_event_params)

    respond_to do |format|
      if @cat_event.save
        format.html { redirect_to @cat_event, notice: "Cat event was successfully created." }
        format.json { render :show, status: :created, location: @cat_event }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @cat_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cat_events/1 or /cat_events/1.json
  def update
    respond_to do |format|
      if @cat_event.update(cat_event_params)
        format.html { redirect_to @cat_event, notice: "Cat event was successfully updated." }
        format.json { render :show, status: :ok, location: @cat_event }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @cat_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cat_events/1 or /cat_events/1.json
  def destroy
    @cat_event.destroy
    respond_to do |format|
      format.html { redirect_to cat_events_url, notice: "Cat event was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cat_event
      @cat_event = CatEvent.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def cat_event_params
      params.require(:cat_event).permit(:trapping_project_id, :cat_id, :cat_event_type_id, :cat_event_outcome_id, :start_date, :end_date)
    end
end

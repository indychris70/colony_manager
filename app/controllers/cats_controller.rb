class CatsController < ApplicationController
  include EventsHelper
  # Authentication with Devise. This line must be after the `protect_from_forgery` call (in application controller).
	before_action :authenticate_user!
  # before_action :require_admin, only: [:destroy,:update_deleted_cats,:deleted_cats]
  before_action :set_colony, only: [:index, :show, :edit, :new, :create, :update, :destroy, :quick_create, :deleted_cats, :update_deleted_cats]
  before_action :set_return_to, only: [:destroy, :update_deleted_cats]
  before_action :set_update_list, only: [:edit, :new]

  def index
    @cats = @colony.cats.not_deleted
  end
  
  def quick_create
    cat = @colony.cats.create!(sex: "unknown")
    
    @colony.num_cats = @colony.cats.not_deleted.size
    @colony.total_inflow = @colony.total_inflow + 1
    @colony.save

    create_colony_pop_event(@colony, COLONY_POP_EVENTS[CAT_ADDED], 1)

    # add cat event
    cat_event_type = CatEventType.find_by(type_name: "Create")
    cat_event_outcome = CatEventOutcome.find_by(name: "Joined Colony")
    cat.cat_events.create(cat_event_type: cat_event_type, cat_event_outcome: cat_event_outcome, start_date: Date.today, end_date: Date.today)

    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    @cat = @colony.cats.find(params[:id])
    @notes = @cat.notes.order(created_at: :desc)
  end

  def new
    @cat = @colony.cats.new
  end

  def create
    @send_email = params[:suppress_email] ? false : true
    @cat = @colony.cats.new(cat_params)

  	note_text = "New cat details added to colony. \n"
    note_text += "Note text: #{params[:note_text]}"

    respond_to do |format|
      if @cat.save
        note = @cat.notes.new
        note.colony_id = @colony.id
        note.note_text = note_text
        note.user = current_user
        note.save
        header = "New cat details added to colony."
        if @send_email
          recips = params[:email_recipients] || []
          recips.each do |email_address|
            NoteMailer.new_note(email_address, note, @colony, header).deliver
          end
        end
        @colony.num_cats = @colony.cats.not_deleted.size
        @colony.save

        # add cat event
        cat_event_type = CatEventType.find_by(type_name: "Create")
        cat_event_outcome = CatEventOutcome.find_by(name: "Joined Colony")
        @cat.cat_events.create(cat_event_type: cat_event_type, cat_event_outcome: cat_event_outcome, start_date: Date.today, end_date: Date.today)

        
        # create colony pop event to add a cat
        event_inflow = 1
        event_outflow = 0
        event_altered = @cat.is_altered ? 1 : 0
        create_colony_pop_event(@colony, COLONY_POP_EVENTS[CAT_ADDED], event_inflow, event_outflow, event_altered)

        format.html { redirect_to [@colony,@cat], notice: 'Cat was successfully created.' }
        format.json { render :show, status: :created, location: @cat }
      else
        format.html { render :new }
        format.json { render json: @cat.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    @cat = @colony.cats.find(params[:id])
  end

  def update
    @send_email = params[:suppress_email] ? false : true
    @cat = @colony.cats.find(params[:id])
    previously_altered = @cat.is_altered
    note_text = "Colony cat details updated. \n"
    note_text += "Note text: #{params[:note_text]}"
    if @cat.update(cat_params)
      note = @cat.notes.new
      note.colony_id = @colony.id
      note.note_text = note_text
      note.user = current_user
      note.save
      header = "Colony cat details updated."
      if @send_email
        recips = params[:email_recipients] || []
        recips.each do |email_address|
          NoteMailer.new_note(email_address, note, @colony, header).deliver
        end
      end
      # create colony pop event if cat sex is updated from an unaltered sex to altered sex or vice versa, to track total altered over time
      event_inflow = 0
      event_outflow = 0
      cat_event_type = CatEventType.find_by(type_name: "Update")
      if !previously_altered && @cat.is_altered
        cat_event_outcome = CatEventOutcome.find_by(name: "Update:Altered")
        event_altered = 1
        description = COLONY_POP_EVENTS[UPDATE_SEX_ALTERED]
        create_colony_pop_event(@colony, description, event_inflow, event_outflow, event_altered)
        @cat.cat_events.create(cat_event_type: cat_event_type, cat_event_outcome: cat_event_outcome, start_date: Date.today, end_date: Date.today)
      elsif previously_altered && !@cat.is_altered
        cat_event_outcome = CatEventOutcome.find_by(name: "Update:Not Altered")
        event_altered = -1
        description = COLONY_POP_EVENTS[UPDATE_SEX_NOT_ALTERED]
        create_colony_pop_event(@colony, description, event_inflow, event_outflow, event_altered)
        @cat.cat_events.create(cat_event_type: cat_event_type, cat_event_outcome: cat_event_outcome, start_date: Date.today, end_date: Date.today)
      end

      redirect_to colony_cat_path(@colony.id,@cat.id), notice: 'Cat was successfully updated.' 
    end
  end
    
  def destroy
    @cat = Cat.find(params[:id])
    if @cat.update(:deleted => true, :deleted_by => current_user.id, :deleted_at => Time.now, :delete_reason => params[:delete_reason])
      
      @colony.num_cats = @colony.cats.not_deleted.size
      @colony.save

      # add a colony pop event to remove a cat from the colony
      create_colony_pop_event(@colony, COLONY_POP_EVENTS[CAT_REMOVED], 0, 1)

      if request.original_fullpath == "/colonies/#{@colony.id}/cats/#{@cat.id}" 
        session.delete(:return_to)
        redirect_action = colony_cats_path
      else 
        redirect_action = session.delete(:return_to)
      end

      respond_to do |format|
        format.html { redirect_to redirect_action, notice: "Cat was successfully deleted." }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { render :edit }
        format.json { render json: @cat.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def deleted_cats
    @cats = @colony.cats.is_deleted
  end
  
  def update_deleted_cats
    # fail
    if params[:cat_ids]
      cats = Cat.id(params[:cat_ids])
      case params[:cat_selected]
      when "purge" then 
        cats.each { |c| c.destroy }
        redirect_to session.delete(:return_to), notice: "Selected cats have been purged."
      when "restore" then
        cats.update_all(:deleted => nil, :deleted_by => nil, :deleted_at => nil, :delete_reason => nil)
        @colony.num_cats = @colony.cats.not_deleted.size
        @colony.save
        redirect_to session.delete(:return_to), notice: "Selected cats have been restored."
      else
        redirect_to session.delete(:return_to), alert: "No action was selected."
      end
    else
      redirect_to session.delete(:return_to), alert: "No cats were selected."
    end
  end

private

    # Use callbacks to share common setup or constraints between actions.
    def set_colony
      begin
        @colony = Colony.find(params[:colony_id])
      rescue Exception => e
        redirect_to root_url, alert: "Oops! There was a problem: #{e}"
      end
    end

    def get_option_data(user)
      is_included = true
      if user.id == @colony.user_id 
        description = "Assigned volunteer"
      elsif user.admin
        description = "Admin user"
      else 
        description = "Non-admin user"
        is_included = false
      end
      return {
        name: user.name, 
        email: user.email, 
        description: description, 
        is_included: is_included
      }
    end

    def set_update_list
      update_list = []

      # Get all users
      users = User.all 
      user_update_list = users.map { |u| get_option_data(u) }

      # Get all colony update role persons
      people = @colony.people.has_receive_updates_role.uniq
      people_update_list = people.map { |pe| { name: pe.full_name, email: pe.email, description: "Colony team", is_included: true } }

      update_list += user_update_list
      update_list += people_update_list

      @update_options = ""
      update_list.each do |option|
        if option[:is_included]
          name = option[:name]
          email = option[:email]
          description = option[:description]
          @update_options += "<option value='#{email}' selected>#{name} (#{description})</option>"
        end
      end
    end
    
# Never trust parameters from the scary internet, only allow the white list through.
    def cat_params
      params.require(:cat).permit(:name, :id_code, :sex, :weight_lbs, :weight_oz, :dob, :dob_certainty, :medical_info, :eartip, :physical_description, :deleted, :deleted_at, :deleted_by, :delete_reason)
    end

end


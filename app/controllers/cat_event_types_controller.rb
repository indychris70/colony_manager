class CatEventTypesController < ApplicationController
  before_action :set_cat_event_type, only: %i[ show edit update destroy ]
  before_action :authenticate_user!
  before_action :require_admin

  # GET /cat_event_types or /cat_event_types.json
  def index
    @cat_event_types = CatEventType.all
  end

  # GET /cat_event_types/1 or /cat_event_types/1.json
  def show
  end

  # GET /cat_event_types/new
  def new
    @cat_event_type = CatEventType.new
  end

  # GET /cat_event_types/1/edit
  def edit
  end

  # POST /cat_event_types or /cat_event_types.json
  def create
    @cat_event_type = CatEventType.new(cat_event_type_params)
    @cat_event_type.created_by = current_user.id

    respond_to do |format|
      if @cat_event_type.save
        format.html { redirect_to @cat_event_type, notice: "Cat event type was successfully created." }
        format.json { render :show, status: :created, location: @cat_event_type }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @cat_event_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cat_event_types/1 or /cat_event_types/1.json
  def update
    respond_to do |format|
      if @cat_event_type.update(cat_event_type_params)
        format.html { redirect_to @cat_event_type, notice: "Cat event type was successfully updated." }
        format.json { render :show, status: :ok, location: @cat_event_type }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @cat_event_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cat_event_types/1 or /cat_event_types/1.json
  def destroy
    @cat_event_type.destroy
    respond_to do |format|
      format.html { redirect_to cat_event_types_url, notice: "Cat event type was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cat_event_type
      @cat_event_type = CatEventType.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def cat_event_type_params
      params.require(:cat_event_type).permit(:type_name, :type_description, :is_active, :deleted_at, :is_deleted, :created_by, :deleted_by, :is_system_type)
    end
end

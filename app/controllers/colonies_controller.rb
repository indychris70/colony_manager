class ColoniesController < ApplicationController
  include EventsHelper
  # Authentication with Devise. This line must be after the `protect_from_forgery` call (in application controller).
	before_action :authenticate_user!, except: [:index]
  before_action :set_colony, only: [:show, :edit, :update, :destroy, :manage_colony_people, :update_colony_person]
  before_action :require_admin, only: [:destroy,:mass_assign,:manage_duplicates,:update_deleted_colonies,:deleted_colonies]
  before_action :set_return_to, only: [:deleted_colonies,:update_deleted_colonies,:create,:destroy,:import]
  before_action :set_update_list, only: [:edit, :new, :show]
  autocomplete :colony, :formatted_address, :full => true

  # GET /colonies
  # GET /colonies.json
  def index
    redirect_to map_path if current_user
  end

  def check_addresses
    @colonies = Colony.not_deleted.geocode_not_precise
    @hash_colonies = get_markers(@colonies,nil,'location_type')
  end

  def manage_duplicates
    duplicate_addresses = Colony.not_deleted.select('formatted_address, count(formatted_address) as count_all').group(:formatted_address).having("count(*)>1")
    dup_address_list = []
    duplicate_addresses.each do |colony|
      dup_address_list.push(colony.formatted_address)
    end
    @duplicate_colonies = Colony.not_deleted.formatted_address(dup_address_list)
  end

  def deleted_colonies
    @colonies = Colony.is_deleted
  end

  def update_deleted_colonies
    # fail
    if params[:colony_ids]
      colonies = Colony.id(params[:colony_ids])
      case params[:colony_selected]
      when "purge" then 
        colonies.each { |c| c.destroy }
        redirect_to session.delete(:return_to), notice: "Selected colonies have been purged."
      when "restore" then
        colonies.update_all(:deleted => nil, :deleted_by => nil, :deleted_at => nil)
        redirect_to session.delete(:return_to), notice: "Selected colonies have been restored."
      else
        redirect_to session.delete(:return_to), alert: "No action was selected."
      end
    else
      redirect_to session.delete(:return_to), alert: "No colonies were selected."
    end
  end

  def mass_assign
  # fail
    if params[:params_present] == 'true'
      @mass_assign_visibility = ""
      
      @results_message = ""
      params.each do |param,value|
        if [[""],""].include?(value)
          params[param] = nil
        else
          @results_message += "#{param}: #{value} " unless ['utf8','action','controller'].include?(param)
        end
      end

      if params[:category_id]
        # fail
        @category = Category.find(params[:category_id][0])
        @colonies = @category.colonies
        @colonies = @colonies.not_deleted.filter_it(params.slice(:county, :zip_code, :city, :township, :neighborhood, 
                                              :min_cats, :max_cats, :status_code_id, :user_id,
                                              :address_contains, :business_name_contains, :colony_name_contains, :max_ins_date, :min_ins_date ))
      else



        @colonies = Colony.not_deleted.filter_it(params.slice(:county, :zip_code, :city, :township, :neighborhood, 
                                          :min_cats, :max_cats, :status_code_id, :user_id,
                                          :address_contains, :business_name_contains, :colony_name_contains, :max_ins_date, :min_ins_date ))
      end

      @params_hash = params
      if ![nil,""].include?(params[:new_user_id]) || ![nil,""].include?(params[:new_status_code_id])
        @colonies.each do |colony| 
          colony = colony.compact.first if colony.respond_to?('size')
          old_colony_user = colony.user ? colony.user.name : "NA"
          old_status_code = colony.status_code ? colony.status_code.status_code : "NA"
          colony.user_id = params[:new_user_id].first.to_i if params[:new_user_id] && params[:new_user_id].size > 0
          colony.status_code_id = params[:new_status_code_id].first.to_i if params[:new_status_code_id] && params[:new_status_code_id].size > 0
          colony.save
          note_text = "MASS UPDATE: \n" 
          if ![nil,""].include?(params[:new_user_id])
            note_text += "   Assigned Volunteer:  Updated from #{old_colony_user} to #{colony.user.name}. \n"
          end
          if ![nil,""].include?(params[:new_status_code_id])
            note_text += "   Status Code:  Updated from #{old_status_code} to #{colony.status_code.status_code}. \n"
          end
          note_text += "   Additional Details: #{params[:note_text]}\n" if params[:note_text]
          colony.notes.create!(:note_text => note_text, :user_id => current_user.id)
        end
        flash.now.notice = "Selected Colonies have been reassigned."
      end
    else
      @mass_assign_visibility = "display:none"
      @params_hash = {}
    end
  end

  def dashboard
    # stats
    @colony_count = Colony.not_deleted.count
    @cat_count = Colony.not_deleted.select(:num_cats).sum(:num_cats)
    @status_codes = StatusCode.not_deleted
    @no_status_code = Colony.not_deleted.no_status_code.size
    @no_assigned_volunteer = Colony.not_deleted.no_assigned_volunteer.size
    @assigned_volunteers = User.not_deleted.is_assigned_volunteer
    @colonies_per_zip = Colony.not_deleted.group(:zip_code).count
    @categories = Category.all
    @duplicates = manage_duplicates
    @check_addresses = check_addresses
    
    
  end

  def map
    puts "params: #{params}"
    # fail
    # session[:test] = @user_filter
    # params = ActionController::Parameters.new(session[:params]) 
    # fail
    # session[:params] = nil
    # session[:count] = nil
    if session[:params]
      session[:count] = 1
      session[:params].each_key do |k|
        params[k] = session[:params][k]
        session[:count] += 1
      end
      # params[:county] = ['Marion County']
      params[:params_present] = 'true'
      session[:params] = nil
    end
    # fail
    if params[:params_present] == 'true' || ( params[:search].present? && params[:search] != "" )
      if params[:make_quick_filter] == "1" && params[:quick_filter_name].size!=0
        params[:make_quick_filter] = ""        
        current_user.saved_filters.create!(name: params[:quick_filter_name], filter_params: params.to_unsafe_h )
      else
        flash.now.alert = "Filter not saved. No name was provided." if params[:make_quick_filter] && params[:make_quick_filter].size!=0
      end
      params[:text_radius] = "" # text box for display only, slider param "radius" is the controlling input
      
      @results_message = ""
      params.each do |param,value|
        if [[""],""].include?(value)
          params[param] = nil
        else
          @results_message += "#{param}: #{value} " unless ['utf8','action','controller','params_present','make_quick_filter','quick_filter_name','status_code_id','user_id'].include?(param)
          @results_message += "#{param}: #{user(value.join).name} " if param == 'user_id'
          @results_message += "#{param}: #{status_code(value.join) ? status_code(value.join).status_code : 'Not Found'} " if param == 'status_code_id'
        end
      end

      @params_hash = params

      if params[:category_id]
        # fail
        @category = Category.find(params[:category_id][0])
        @colonies = @category.colonies
        @colonies = @colonies.not_deleted.filter_it(params.slice(:county, :zip_code, :city, :township, :neighborhood, 
                                              :min_cats, :max_cats, :status_code_id, :user_id,
                                              :address_contains, :business_name_contains, :colony_name_contains, :max_ins_date, :min_ins_date ))
      else
        # fail

        @colonies = Colony.not_deleted.filter_it(params.slice(:county, :zip_code, :city, :township, :neighborhood, 
                                              :min_cats, :max_cats, :status_code_id, :user_id,
                                              :address_contains, :business_name_contains, :colony_name_contains, :max_ins_date, :min_ins_date ))
      end

      formatted_addresses = @colonies.pluck(:formatted_address).uniq

      if params[:search].present? && params[:search] != ""
        @colonies = @colonies.near(params[:search], params[:radius].to_f)

        # Get list of people with available roles
        @people = Person.with_available_roles.near(params[:search], params[:radius].to_f).uniq

        @header = "Search Results: Colonies - #{@colonies.size}, People - #{@people.size}"
        @search = true
      else
        # @colonies = @colonies.order('created_at desc')
        @header = "Showing #{@colonies.size} Colonies Total"

        max_latitude = -90
        max_longitude = -180
        min_latitude = 90
        min_longitude = 180

        @colonies.each do |colony|
          max_latitude = colony.latitude if colony.latitude > max_latitude
          max_longitude = colony.longitude if colony.longitude > max_longitude
          min_latitude = colony.latitude if colony.latitude < min_latitude
          min_longitude = colony.longitude if colony.longitude < min_longitude 
        end

        @people = Person.with_available_roles.within_bounding_box([min_latitude - 0.01, min_longitude - 0.01], [max_latitude + 0.01, max_longitude + 0.01]).uniq
      end

      @hash = get_people_markers(@people, formatted_addresses)
      @hash += get_markers(@colonies)

      

      puts "v"*88
      puts @hash 
      puts "^"*88
      @status_codes = StatusCode.all

        respond_to do |format|
          format.html
          format.csv { send_data @colonies.to_csv, :type => 'text/csv; charset=iso-8859-1; header=present', :disposition => "attachment; filename=colonies.csv" } if current_user_admin?
        end
    else
      @colonies = Colony.not_deleted.created_last_x_days(30)
      @header = "#{@colonies.size} Colonies Created in the Last 30 Days"
      @params_hash = {}
      @hash = get_markers(@colonies)
      @status_codes = StatusCode.all
      respond_to do |format|
          format.html
          format.csv { send_data @colonies.to_csv, :type => 'text/csv; charset=iso-8859-1; header=present', :disposition => "attachment; filename=colonies.csv" } if current_user_admin?
      end
    end
  end

  # GET /colonies/1
  # GET /colonies/1.json
  def show
    params[:nearbys_page] ? nearbys_page = params[:nearbys_page] : nearbys_page = 1
    params[:notes_page] ? notes_page = params[:notes_page] : notes_page = 1
    @colony_nearbys = @colony.nearbys(3).not_deleted
    @hash_nearbys = get_markers(@colony_nearbys)
    # formatted_addresses lines needs to go after the first time @colony_nearbys is used
    formatted_addresses = @colony_nearbys.pluck(:formatted_address)
    formatted_addresses << @colony.formatted_address 
    formatted_addresses = formatted_addresses.uniq
    @people_resources = Person.with_available_roles.near(@colony.formatted_address, 3).uniq
    @hash_people = get_people_markers(@people_resources, formatted_addresses)
    # @colony_nearbys = @colony_nearbys.paginate(:page => nearbys_page, :per_page => 10)
    @hash_colony = get_markers(@colony, @colony)
    @notes = @colony.notes.order(created_at: :desc)
    @colony.user.nil? ? @field_volunteer = "Unassigned" : @field_volunteer = @colony.user.name
    @colony.status_code.nil? ? @status_code = "Unassigned" : @status_code = @colony.status_code.status_code
    @categories = @colony.categories
    @status_codes = StatusCode.all
    @hash_colonies = @hash_people + @hash_nearbys + @hash_colony
    @people = @colony.people.uniq
    @trapping_projects = @colony.trapping_projects
  end

  # GET /colonies/new
  def new
    @colony = Colony.new
  end

  # GET /colonies/1/edit
  def edit
    @people = @colony.people.uniq
  end

  # POST /colonies
  # POST /colonies.json
  def create
    @send_email = params[:suppress_email] ? false : true
    @colony = Colony.new(colony_params)
    @colony.status_code_change_date = Time.now
    @colony.ins_date = Time.now.strftime('%Y-%m-%d')
    @colony.original_population = params[:colony][:num_cats].to_i || 0
    note_text = ""
    @colony.user_id.nil? ? note_text += "NEW Colony was not assigned to a volunteer.\n" : note_text += "NEW Colony assigned to: #{user(params[:colony][:user_id]).name}\n"
    note_text += "NEW Colony status: #{status_code(params[:colony][:status_code_id]).status_code}\n"
    note_text += "Note text: #{params[:note_text]}"
    header = "A colony has been created by #{current_user}."
    note = @colony.notes.new
    note.note_text = note_text
    note.user = current_user
    note.save
    if @send_email
      recips = params[:email_recipients] || []
      recips.each do |email_address|
        NoteMailer.new_note(email_address, note, @colony, header).deliver
      end
    end
    
    respond_to do |format|
      if @colony.save
        (params[:colony][:num_cats].to_i).times do
          @colony.cats.create!
        end
        event_inflow = params[:colony][:num_cats]
        create_colony_pop_event(@colony, COLONY_POP_EVENTS[NEW_COLONY], event_inflow)
        format.html { redirect_to manage_colony_people_path(@colony), notice: "Colony successfully created. You may now add people to the colony team." }
        format.json { render :show, status: :created, location: @colony }
      else
        format.html { render :new }
        format.json { render json: @colony.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /colonies/1
  # PATCH/PUT /colonies/1.json
  def update
    important_changes = false # if colony status or assigned user changes, update this to true to indicate an email should be sent
    @send_email = params[:suppress_email] ? false : true
    if params[:colony][:status_code_id] != "" && params[:colony][:status_code_id].to_i != @colony.status_code_id
      @colony.status_code_change_date = Time.now
    end
    original_user_id = @colony.user_id
    original_status_code_id = @colony.status_code_id
    if @colony.update(colony_params)
      note_text = ""
      if @colony.user_id != original_user_id
        if create_colony_pop_event(@colony, COLONY_POP_EVENTS[ASSIGNED_USER_CHANGE])
          note_text += "Colony assigned to: #{user(@colony.user_id).name} \n"
          important_changes = true
        end
      else 
        note_text += "Colony was not reassigned.\n"
      end
      
      if @colony.status_code_id != original_status_code_id
        if create_colony_pop_event(@colony, COLONY_POP_EVENTS[STATUS_CHANGE])
          note_text += "Status updated to: #{status_code(@colony.status_code_id).status_code} \n"
          important_changes = true
        end
      else 
        note_text += "Status was not updated.\n"
      end
      note_text += "Note text: #{params[:note_text]}" if params[:note_text] && params[:note_text].size>0
      if important_changes || params[:note_text] != ""
        header = "A colony has been updated by #{current_user.name.empty? ? current_user.email : current_user.name}."
        note = @colony.notes.new
        note.note_text = note_text
        note.user = current_user
        note.save
        if @send_email
          recips = params[:email_recipients] || []
          recips.each do |email_address|
            NoteMailer.new_note(email_address, note, @colony, header).deliver
          end
        end
      end
      respond_to do |format|
        format.html { redirect_to colony_path(@colony.id), notice: 'Colony was successfully updated.' }
        format.json { render :show, status: :ok, location: @colony }
      end
    else
      respond_to do |format|
        format.html { render :edit }
        format.json { render json: @colony.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /colonies/1
  # DELETE /colonies/1.json
  def destroy
    # @colony.destroy
    if @colony.update(:deleted => true, :deleted_by => current_user.id, :deleted_at => Time.now)

      if request.original_fullpath == "/colonies/#{@colony.id}" 
        session.delete(:return_to)
        redirect_action = map_path
      else 
        redirect_action = session.delete(:return_to)
      end

      respond_to do |format|
        format.html { redirect_to redirect_action, notice: "Colony was successfully deleted." }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { render :edit }
        format.json { render json: @colony.errors, status: :unprocessable_entity }
      end
    end
  end

  def import
    Colony.import(params[:file])
    redirect_to session.delete(:return_to), notice: "Upload Successful."
  end

  def summary_report
    @results = Colony.not_deleted.group(params[:group_by]).sum(:num_cats).sort_by {|k,v| v}.reverse if params[:group_by]
  end

  def manage_colony_people
    @people = @colony.people.uniq
    @role_names = RoleValue.pluck(:name)
  end

  def update_colony_person
    role_name = params[:role]
    role_value = RoleValue.find_by(:name => role_name)
    person = Person.find(params[:person_id])
    person_roles_at_colony = person.roles.where(:colony_id => @colony.id)
    if person_roles_at_colony.pluck(:role_value_id).include?(role_value.id)
      person_roles_at_colony.where(:role_value_id => role_value.id).delete_all
      action_taken = "deleted from"
    else
      person.roles.create(colony: @colony, role_value: role_value, name: role_name)
      action_taken = "added to"
    end
    person_removed_from_list_message = person.roles.where(:colony_id => @colony.id).size <= 0 ? "#{person.first_name} has no more roles at this colony and has been removed from the list." : ""

    redirect_to manage_colony_people_path(@colony), notice: "The #{role_name} role was #{action_taken} #{person.first_name}. #{person_removed_from_list_message}"
  end

  def search 
  end

  def search_selection 
    if params[:colony_id].empty?
      redirect_to new_colony_path
    else
      @colony = Colony.find(params[:colony_id])
      redirect_to colony_path(@colony)
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_colony
      begin
        @colony = Colony.find(params[:id])
      rescue Exception => e
        redirect_to root_url, alert: "Oops! There was a problem: #{e}"
      end
    end

    def get_option_data(user)
      is_included = true
      if user.id == @colony.user_id 
        description = "Assigned volunteer"
      elsif user.admin
        description = "Admin user"
      else 
        description = "Non-admin user"
        is_included = false
      end
      return {
        name: user.name, 
        email: user.email, 
        description: description, 
        is_included: is_included
      }
    end

    def set_update_list
      update_list = []

      # Get all users
      users = User.all 
      user_update_list = users.map { |u| get_option_data(u) }

      # Get all colony update role persons
      people = @colony.people.has_receive_updates_role.uniq
      people_update_list = people.map { |pe| { name: pe.full_name, email: pe.email, description: "Colony team", is_included: true } }

      update_list += user_update_list
      update_list += people_update_list

      @update_options = ""
      update_list.each do |option|
        if option[:is_included]
          name = option[:name]
          email = option[:email]
          description = option[:description]
          @update_options += "<option value='#{email}' selected>#{name} (#{description})</option>"
        end
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def colony_params
      params.require(:colony).permit(:colony_name, :business_name, :address1, :address2, :city, :state, :zip_code,
        :num_cats, :latitude, :longitude, :neighborhood, :county, :township, :formatted_address, :status_code_id, :user_id,
        :ins_date, :legacy_people_id, :legacy_colony_id, :location_type, :legacy_cc_assigned, category_ids: [])
    end
    

end

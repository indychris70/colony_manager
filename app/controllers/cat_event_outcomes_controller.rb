class CatEventOutcomesController < ApplicationController
  before_action :set_cat_event_outcome, only: %i[ show edit update destroy ]
  before_action :authenticate_user!
  before_action :require_admin

  # GET /cat_event_outcomes or /cat_event_outcomes.json
  def index
    @cat_event_outcomes = CatEventOutcome.all
  end

  # GET /cat_event_outcomes/1 or /cat_event_outcomes/1.json
  def show
  end

  # GET /cat_event_outcomes/new
  def new
    @cat_event_outcome = CatEventOutcome.new
  end

  # GET /cat_event_outcomes/1/edit
  def edit
  end

  # POST /cat_event_outcomes or /cat_event_outcomes.json
  def create
    @cat_event_outcome = CatEventOutcome.new(cat_event_outcome_params)
    @cat_event_outcome.created_by = current_user.id

    respond_to do |format|
      if @cat_event_outcome.save
        format.html { redirect_to @cat_event_outcome, notice: "Cat event outcome was successfully created." }
        format.json { render :show, status: :created, location: @cat_event_outcome }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @cat_event_outcome.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cat_event_outcomes/1 or /cat_event_outcomes/1.json
  def update
    respond_to do |format|
      if @cat_event_outcome.update(cat_event_outcome_params)
        format.html { redirect_to @cat_event_outcome, notice: "Cat event outcome was successfully updated." }
        format.json { render :show, status: :ok, location: @cat_event_outcome }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @cat_event_outcome.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cat_event_outcomes/1 or /cat_event_outcomes/1.json
  def destroy
    @cat_event_outcome.destroy
    respond_to do |format|
      format.html { redirect_to cat_event_outcomes_url, notice: "Cat event outcome was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cat_event_outcome
      @cat_event_outcome = CatEventOutcome.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def cat_event_outcome_params
      params.require(:cat_event_outcome).permit(:name, :description, :is_active, :deleted_at, :is_deleted, :created_by, :deleted_by, :is_system_outcome, :remove_cat)
    end
end

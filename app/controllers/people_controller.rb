class PeopleController < ApplicationController
    before_action :authenticate_user!
    before_action :set_person, only: [:show, :edit, :update, :destroy, :update_person_roles]
    before_action :set_return_to, only: [:deleted_people,:update_deleted_people,:destroy]
    autocomplete :person, :full_name, :full => true

    def search_selection 
        if params[:person_id].empty?
            redirect_to new_person_path
        else
            @person = Person.find(params[:person_id])
            redirect_to person_path(@person)
        end
    end

    def search
    end

    def index
        @people = Person.not_deleted
        respond_to do |format|
            format.html
            format.csv { send_data @people.to_csv, :type => 'text/csv; charset=iso-8859-1; header=present', :disposition => "attachment; filename=people.csv" } if current_user_admin?
        end
    end

    def show 
        @categories = @person.categories
    end

    def new 
        @person = Person.new 
    end

    def create
        puts "params: #{params}"
        @person = Person.new(person_params)
        @person.full_name = "#{@person.last_name}, #{@person.first_name}"

        respond_to do |format|
            if @person.save
                # Add roles
                role_value_ids = params[:person][:role_value_ids]
                role_value_ids && role_value_ids.each do |rvid|
                    if rvid != ""
                        role_value = RoleValue.find(rvid)
                        if role_value
                            @person.roles.create(role_value: role_value, name: role_value.name) unless @person.roles.available.pluck(:role_value_id).include?(rvid.to_s)
                        end
                    end
                end
                format.html { redirect_to person_path(@person.id), notice: 'Person was successfully created.' }
                format.json { render :show, status: :created, location: @person }
            else
                format.html { render :new }
                format.json { render json: @person.errors, status: :unprocessable_entity }
            end
        end
    end

    def edit
    end

    def update
        params["person"]["full_name"] = "#{person_params['last_name']}, #{person_params['first_name']}" if params["person"]
        respond_to do |format|
            if @person.update(person_params)

                # update roles
                role_value_ids = params[:person][:role_value_ids]

                RoleValue.all.each do |role_value|
                    # add role unless it already exists
                    if role_value_ids.include?(role_value.id.to_s)
                        @person.roles.create(role_value: role_value, name: role_value.name) unless @person.roles.available.pluck(:role_value_id).include?(role_value.id)
                    else # remove role if it exists
                        roll_to_remove = @person.roles.available.find_by(:role_value_id=>role_value.id)
                        roll_to_remove.delete if roll_to_remove 
                    end
                end

                format.html { redirect_to @person, notice: 'Person was successfully updated.' }
                format.json { render :show, status: :ok, location: @person }
            else
                format.html { render :edit }
                format.json { render json: @person.errors, status: :unprocessable_entity }
            end
        end
    end

    def destroy 
        if @person.update(:deleted => true, :deleted_by => current_user.id, :deleted_at => Time.now)

            if request.original_fullpath == "/people/#{@person.id}" 
                session.delete(:return_to)
                redirect_action = people_path
            else 
                redirect_action = session.delete(:return_to)
            end
      
            respond_to do |format|
                format.html { redirect_to redirect_action, notice: "Person was successfully deleted." }
                format.json { head :no_content }
            end
        else
            respond_to do |format|
                format.html { render :edit }
                format.json { render json: @person.errors, status: :unprocessable_entity }
            end
        end
    end

    def deleted_people
        @people = Person.is_deleted 
    end

    def update_deleted_people
        # fail
        if params[:person_ids]
            people = Person.id(params[:person_ids])
            case params[:person_selected]
            when "purge" then 
                people.each { |p| p.destroy }
                redirect_to session.delete(:return_to), notice: "Selected people have been purged."
            when "restore" then
                people.update_all(:deleted => nil, :deleted_by => nil, :deleted_at => nil)
                redirect_to session.delete(:return_to), notice: "Selected people have been restored."
            else
                redirect_to session.delete(:return_to), alert: "No action was selected."
            end
        else
            redirect_to session.delete(:return_to), alert: "No people were selected."
        end
    end

    def update_person_roles
        puts "params: #{params}"
        role_value_id = params[:role_value_id]
        role_value = RoleValue.find(role_value_id)
        role_name = role_value.name

        if params[:role] 
            action_taken = "added to"
            @person.roles.create(role_value: role_value, name: role_name) unless @person.roles.available.pluck(:role_value_id).include?(role_value_id)
        else 
            action_taken = "removed from"
            existing_role = @person.roles.available.find_by(role_value_id: role_value_id)
            existing_role.delete if existing_role
        end

        redirect_to person_path(@person), notice: "The #{role_name} role was #{action_taken} #{@person.first_name}."
    end

private

    def set_person
        begin
            @person = Person.find(params[:id])
        rescue Exception => e
            redirect_to people_path, alert: "Oops! There was a problem: #{e}"
        end
    end

    def person_params
        params.require(:person).permit(:first_name, :last_name, :full_name, :address1, :address2, :city, :state, :zip_code, :phone_description,
          :phone, :alt1_phone_desc, :alt1_phone, :email_description, :email, :alt1_email_desc, :alt1_email, :latitude, :longitude,
          :neighborhood, :county, :township, :formatted_address, :alt2_phone, :alt2_phone_desc, :location_type, category_ids: [])
      end
end
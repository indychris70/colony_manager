json.extract! colony_pop_event, :id, :event_inflow, :event_outflow, :total_inflow, :total_outflow, :altered_population, :colony_id, :trapping_project_id, :colony_population_event_type_id, :created_at, :updated_at
json.url colony_pop_event_url(colony_pop_event, format: :json)

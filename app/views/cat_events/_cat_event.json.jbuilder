json.extract! cat_event, :id, :trapping_project_id, :cat_id, :cat_event_type_id, :cat_event_outcome_id, :start_date, :end_date, :created_at, :updated_at
json.url cat_event_url(cat_event, format: :json)

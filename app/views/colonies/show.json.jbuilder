json.extract! @colony, :id, :first_name, :last_name, :address1, :address2, :city, :state, :zip_code, :phone, :email, :num_cats, :latitude, :longitude, :created_at, :updated_at

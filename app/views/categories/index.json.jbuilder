json.array!(@categories) do |category|
  json.extract! category, :id, :name, :description, :deleted, :deleted_by, :deleted_at
  json.url category_url(category, format: :json)
end

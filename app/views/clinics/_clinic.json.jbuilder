json.extract! clinic, :id, :name, :contact1, :contact2, :phone1, :phone2, :email1, :email2, :active, :deleted, :deleted_at, :deleted_by, :address1, :address2, :city, :state, :zip_code, :description, :created_at, :updated_at
json.url clinic_url(clinic, format: :json)

json.extract! colony_population_event_type, :id, :name, :description, :is_active, :deleted_at, :is_deleted, :created_by, :deleted_by, :is_system_type, :created_at, :updated_at
json.url colony_population_event_type_url(colony_population_event_type, format: :json)

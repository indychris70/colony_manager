json.extract! cat_event_outcome, :id, :name, :description, :is_active, :deleted_at, :is_deleted, :created_by, :deleted_by, :is_system_outcome, :created_at, :updated_at
json.url cat_event_outcome_url(cat_event_outcome, format: :json)

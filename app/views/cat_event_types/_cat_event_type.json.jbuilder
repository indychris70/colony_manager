json.extract! cat_event_type, :id, :type_name, :type_description, :is_active, :deleted_at, :is_deleted, :created_by, :deleted_by, :is_system_type, :created_at, :updated_at
json.url cat_event_type_url(cat_event_type, format: :json)

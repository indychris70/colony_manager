json.array!(@status_codes) do |status_code|
  json.extract! status_code, :id, :status_code, :is_active, :created_by, :deleted_by, :deleted_at
  json.url status_code_url(status_code, format: :json)
end

json.extract! trapping_project, :id, :scheduled_date, :actual_date, :planned_cats, :actual_cats, :assigned_user, :user_id, :colony_id, :created_at, :updated_at
json.url trapping_project_url(trapping_project, format: :json)

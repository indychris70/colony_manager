class NoteMailer < ActionMailer::Base
  default from: "#{ENV['REPLY_EMAIL_ADDRESS']}"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.note_mailer.new_note.subject
  #
  def new_note(to_email,note,colony,header)
    @header = header
    @note = note
    @colony = colony
    excluded_email_addresses = ["admin@admin.com", "admin@email.com"]

    # if !ENV["HOST_URL"] || ENV["HOST_URL"] == "colony-manager-demo-staging.herokuapp.com" || ENV["HOST_URL"] == "colony-manager-demo.herokuapp.com"
    #   recipients << ENV["DEVELOPER_EMAIL"] unless recipients.include?(ENV["DEVELOPER_EMAIL"])
    # elsif colony && colony.user && colony.user.email 
    #   recipients << colony.user.email unless recipients.include?(colony.user.email)
    # end

    # puts "recipients: "
    # puts recipients

    # recipients.uniq.each do |email_address|
      puts excluded_email_addresses.include?(to_email) ? "Not sending email to #{to_email}" : "sending email to #{to_email}"
      mail to: to_email, subject: "#{ENV['APPLICATION_NAME']}: New note added to colony" unless excluded_email_addresses.include?(to_email)
    # end
  end
end

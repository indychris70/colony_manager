class Clinic < ApplicationRecord
    has_and_belongs_to_many :trapping_projects
    has_many :cat_events
    has_many :notes
end

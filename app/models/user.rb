class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :lockable, :timeoutable, :omniauthable, :confirmable
  devise :invitable, :database_authenticatable, :registerable,
		 :recoverable, :rememberable, :validatable, :trackable 
		 
  has_many :notes
  has_many :colonies
  has_many :saved_filters, dependent: :destroy
  has_many :invitations, :class_name => self.to_s, :as => :invited_by
  has_many :colony_pop_events
  
#   has_secure_password
#   validates :name, presence: true
#   validates :email, presence: true, format: /\A\S+@\S+\z/, uniqueness: { case_sensitive: false }
#   validates :password, length: { minimum: 4, allow_blank: true }

	# def self.authenticate(email, password)
	#   user = User.find_by(email: email)
	#   user && user.authenticate(password)
	# end

	scope :email, -> (email) { where email: email }
	scope :is_admin, -> { where admin: true }
	scope :not_deleted, -> { where('deleted is null or deleted is false') }
	scope :is_deleted, -> { where(:deleted => true) }
	scope :is_assigned_volunteer, -> { where(:field_volunteer => true) }
end

class Category < ActiveRecord::Base
	has_many :categorizations, dependent: :destroy
	has_many :colonies, through: :categorizations
	has_many :people, through: :categorizations

	scope :for_colonies, -> { where model: "Colony" }
	scope :for_people, -> { where model: "Person" }
end

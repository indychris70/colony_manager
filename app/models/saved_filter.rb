class SavedFilter < ActiveRecord::Base
  belongs_to :user

  serialize :filter_params, Hash
end

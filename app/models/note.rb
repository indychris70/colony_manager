class Note < ActiveRecord::Base
  	belongs_to :user
  	belongs_to :colony
	belongs_to :cat
	belongs_to :trapping_project
	belongs_to :clinic
  	
  	validates :note_text, length: { minimum: 1 }

  	scope :not_deleted, -> { where('deleted is null') }
	scope :is_deleted, -> { where(:deleted => true) }
end

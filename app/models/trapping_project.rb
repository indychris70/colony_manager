class TrappingProject < ApplicationRecord
  belongs_to :user
  belongs_to :colony
  has_many :notes, dependent: :destroy
  has_many :cat_events
  has_one :colony_pop_event
  has_and_belongs_to_many :clinics
  has_many :cats, through: :cat_events

  def past_scheduled_date?
    return !self.is_finalized && self.scheduled_date && self.scheduled_date < Date.today # .strftime("%Y-%m-%d")
  end

  scope :finalized, -> { where(:is_finalized => true) }
  scope :not_finalized, -> { where(:is_finalized => false) }
  scope :scheduled, -> { where( "is_finalized is false and scheduled_date is not null" ) }
  scope :not_ready, -> { where( "is_finalized is false and user_id is null or scheduled_date is null" ) }
  scope :trapper, -> (user_id) { where user_id: user_id }
  scope :filter_ids, -> (ids) { where(:id => ids)}
  scope :scheduled_on_or_after, -> (date) { where("scheduled_date >= ?", date)}
  scope :scheduled_on_or_before, -> (date) { where("scheduled_date <= ?", date)}
  scope :actual_on_or_after, -> (date) { where("actual_date >= ?", date)}
  scope :actual_on_or_before, -> (date) { where("actual_date <= ?", date)}
end

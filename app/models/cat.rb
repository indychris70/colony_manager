class Cat < ActiveRecord::Base
  belongs_to :colony
  has_many :notes, dependent: :destroy
  has_many :cat_events

  def is_altered
    return ['spayed', 'neutered'].include?(self.sex)
  end
  
  scope :not_deleted, -> { where(:deleted => [false,nil]) }
	scope :is_deleted, -> { where(:deleted => true) }
	scope :id, -> (id) { where id: id }
  scope :altered, -> { where(:sex => ['spayed', 'neutered']) }
end

class ColonyPopulationEventType < ApplicationRecord
    # find system events
    def self.get_trapping_project_event_id
        event_type = self.find_by(:name => "Trapping Project")
        return event_type.id 
    end
end

class CatEvent < ApplicationRecord
  belongs_to :trapping_project
  belongs_to :cat
  belongs_to :cat_event_type
  belongs_to :cat_event_outcome
  belongs_to :clinic
end

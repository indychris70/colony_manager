class StatusCode < ActiveRecord::Base
	has_many :colonies
	has_many :colony_pop_events

	validates :status_code, uniqueness: true

	scope :not_deleted, -> { where('deleted is null') }
	scope :is_deleted, -> { where(:deleted => true) }
	scope :is_active, -> { where(:is_active => true) }
end

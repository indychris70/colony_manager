class Role < ApplicationRecord
  belongs_to :colony
  belongs_to :person
  belongs_to :role_value

  scope :available, -> {where(:colony_id=>nil)} 
  scope :for_colony, -> {where("colony_id is not null")}
end

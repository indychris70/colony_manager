class Person < ApplicationRecord
    has_many :roles, dependent: :destroy
	has_many :role_values, through: :roles
	has_many :colonies, through: :roles
	has_many :categorizations, dependent: :destroy
	has_many :categories, through: :categorizations

    after_validation :geocode, if: ->(obj){ obj.geocoder_address_changed? } 
    validates :address1, presence: true
	validates :city, presence: true
	validates :state, presence: true

    geocoded_by :address do |obj,results|
		puts results.inspect
		puts "*"*88
		puts obj.inspect 
		puts "-"*88
		if geo = results.first
			obj.latitude = geo.latitude
			obj.longitude = geo.longitude
			obj.formatted_address = geo.data['formatted_address']
			obj.location_type = geo.data['geometry']['location_type']
			obj.address1 = ""
			geo.data['address_components'].each do |component|
				case component['types'][0]
					when 'street_number' then obj.address1 += "#{component['long_name']} "
					when 'route' then obj.address1 += component['long_name']
					when 'neighborhood' then obj.neighborhood = component['long_name']
					when 'locality' then obj.city = component['long_name']
					when 'administrative_area_level_3' then obj.township = component['long_name']
					when 'administrative_area_level_2' then obj.county = component['long_name']
					when 'administrative_area_level_1' then obj.state = component['short_name']
					when 'postal_code' then obj.zip_code = component['long_name']
				end
			end
		end
    end
    
    def address
		address_array = []
		address_array.push(address1)
		address_array.push(address2) unless address2
		address_array.push(city)
		address_array.push(state)
		address_array.push(zip_code)

		address_array.compact.join(', ')
    end

    def geocoder_address_changed?
        return self.address.present? && ( self.address1_changed? || self.address2_changed? || self.city_changed? || self.state_changed? || self.zip_code_changed? )
    end
    
    def get_colonies_by_role(role_name)
		matches = []
		self.roles.each do |role|
			matches << role.colony if role.name and role.name == role_name 
		end
		return matches
	end

	def roles_at_colony(colony_id) 
		roles = self.roles 
		role_names = []
		roles && roles.each do |role|
			role_names << role.role_value.name if role.colony_id == colony_id
		end
		return role_names
	end

	def self.to_csv(records=[], options={})
		CSV.generate do |csv|
			csv << column_names
			records = Person.all if records == []
			records.each do |record|
				csv << record.attributes.values_at(*column_names)
			end
		end
	end

	scope :not_deleted, -> { where('people.deleted is null or people.deleted = false') }
	scope :is_deleted, -> { where(:deleted => true) }
	scope :id, -> (id) { where id: id }
	scope :with_available_roles, -> { joins(:roles).where('roles.colony_id is null') }
	scope :has_receive_updates_role, -> { joins(:roles).where('roles.name = "Receive Updates"')}
end

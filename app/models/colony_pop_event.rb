class ColonyPopEvent < ApplicationRecord
  belongs_to :colony
  belongs_to :trapping_project
  belongs_to :colony_population_event_type
  belongs_to :user 
  belongs_to :status_code
end

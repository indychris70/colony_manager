class Colony < ActiveRecord::Base
	include Filterable
	has_many :cats, dependent: :destroy
	has_many :notes, dependent: :destroy
	has_many :categorizations, dependent: :destroy
	has_many :categories, through: :categorizations
	has_many :roles, dependent: :destroy
	has_many :people, through: :roles
	has_many :trapping_projects, dependent: :destroy
	has_many :colony_pop_events, dependent: :destroy
	belongs_to :status_code
	belongs_to :user
	geocoded_by :address do |obj,results|
		puts results.inspect
		puts "*"*88
		puts obj.inspect 
		puts "-"*88
		if geo = results.first
			obj.latitude = geo.latitude
			obj.longitude = geo.longitude
			obj.formatted_address = geo.data['formatted_address']
			obj.location_type = geo.data['geometry']['location_type']
			obj.address1 = ""
			geo.data['address_components'].each do |component|
				case component['types'][0]
					when 'street_number' then obj.address1 += "#{component['long_name']} "
					when 'route' then obj.address1 += component['long_name']
					when 'neighborhood' then obj.neighborhood = component['long_name']
					when 'locality' then obj.city = component['long_name']
					when 'administrative_area_level_3' then obj.township = component['long_name']
					when 'administrative_area_level_2' then obj.county = component['long_name']
					when 'administrative_area_level_1' then obj.state = component['short_name']
					when 'postal_code' then obj.zip_code = component['long_name']
				end
			end
		end
	end

	def self.import(file)
		CSV.foreach(file.path, headers: true) do |row|
			Colony.create! row.to_hash
		end
	end

	# {"address_components"=>[
	# 	{"long_name"=>"5040", "short_name"=>"5040", "types"=>["street_number"]} , 
	# 	{"long_name"=>"Harpers Lane", "short_name"=>"Harpers Ln", "types"=>["route"]} , 
	# 	{"long_name"=>"Fieldstone and Brookstone", "short_name"=>"Fieldstone and Brookstone", "types"=>["neighborhood", "political"]} , 
	# 	{"long_name"=>"Indianapolis", "short_name"=>"Indianapolis", "types"=>["locality", "political"]} , 
	# 	{"long_name"=>"Pike", "short_name"=>"Pike", "types"=>["administrative_area_level_3", "political"]} , 
	# 	{"long_name"=>"Marion County", "short_name"=>"Marion County", "types"=>["administrative_area_level_2", "political"]} , 
	# 	{"long_name"=>"Indiana", "short_name"=>"IN", "types"=>["administrative_area_level_1", "political"]} , 
	# 	{"long_name"=>"United States", "short_name"=>"US", "types"=>["country", "political"]} , 
	# 	{"long_name"=>"46268", "short_name"=>"46268", "types"=>["postal_code"]} , 
	# 	{"long_name"=>"4474", "short_name"=>"4474", "types"=>["postal_code_suffix"]}
	# 	] , 
	# 	"formatted_address"=>"5040 Harpers Lane, Indianapolis, IN 46268, USA", 
	# 	"geometry"=>{"location"=>{"lat"=>39.879017, "lng"=>-86.24723999999999}, 
	# 				"location_type"=>"ROOFTOP", 
	# 				"viewport"=>{"northeast"=>{"lat"=>39.8803659802915, "lng"=>-86.2458910197085} , 
	# 							"southwest"=>{"lat"=>39.8776680197085, "lng"=>-86.2485889802915} 
	# 							}
	# 				}, 
	# 	"place_id"=>"ChIJm7OxpIJVa4gRPmss5ELqGUc", 
	# 	"types"=>["street_address"]
	# }
	
	after_validation :geocode, if: ->(obj){ obj.geocoder_address_changed? } 
	validates :address1, presence: true
	validates :city, presence: true
	validates :state, presence: true
	validates :status_code_id, presence: true

	#validates :zip_code, numericality: { only_interger: true }

	def address
		address_array = []
		address_array.push(address1)
		address_array.push(address2) unless address2
		address_array.push(city)
		address_array.push(state)
		address_array.push(zip_code)

		address_array.compact.join(', ')
	end

	def geocoder_address_changed?
        return self.address.present? && ( self.address1_changed? || self.address2_changed? || self.city_changed? || self.state_changed? || self.zip_code_changed? )
    end

	def self.to_csv(records=[], options={})
		CSV.generate do |csv|
			csv << column_names
				records = Colony.all if records == []
				records.each do |record|
					csv << record.attributes.values_at(*column_names)
				end
		end
	end

	def get_people_by_role(role_name)
		matches = []
		self.roles.each do |role|
			matches << role.person if role.name and role.name == role_name 
		end
		return matches
	end

	def add_cat 
		cat = self.cats.create!(sex: "unknown")
		self.num_cats = self.cats.not_deleted.count 
		self.total_inflow += 1
		self.save 

		# add cat event
		cat_event_type = CatEventType.find_by(type_name: "Create")
		cat_event_outcome = CatEventOutcome.find_by(name: "Joined Colony")
		cat.cat_events.create(cat_event_type: cat_event_type, cat_event_outcome: cat_event_outcome, start_date: Date.today, end_date: Date.today)
	end

	def altered_population 
		return self.cats.not_deleted.altered 
	end

	scope :county, -> (county) { where county: county }
	scope :city, -> (city) { where city: city }
	scope :zip_code, -> (zip_code) { where zip_code: zip_code }
	scope :id, -> (id) { where id: id }
	scope :township, -> (township) { where township: township }
	scope :neighborhood, -> (neighborhood) { where neighborhood: neighborhood }
	scope :formatted_address, -> (formatted_address) { where formatted_address: formatted_address }
	scope :min_cats, -> (num_cats) { where("num_cats >= ?", num_cats) }
	scope :max_cats, -> (num_cats) { where("num_cats <= ?", num_cats) }
	scope :min_ins_date, -> (ins_date) { where("ins_date >= ?", ins_date) }
	scope :max_ins_date, -> (ins_date) { where("ins_date <= ?", ins_date) }
	scope :address_contains, -> (address_fragment) { where("address1 like ?", "%#{address_fragment}%") }
	scope :business_name_contains, -> (business_name_fragment) { where("business_name like ?", "%#{business_name_fragment}%") }
	scope :colony_name_contains, -> (colony_name_fragment) { where("colony_name like ?", "%#{colony_name_fragment}%") }
	scope :not_deleted, -> { where('colonies.deleted is null or colonies.deleted is false') }
	scope :is_deleted, -> { where(:deleted => true) }
	scope :status_code_id, -> (status_code_id) { where status_code_id: status_code_id }
	scope :user_id, -> (user_id) { where user_id: user_id }
	scope :no_status_code, -> { where(:status_code_id => nil) }
	scope :no_assigned_volunteer, -> { where(:user_id => nil) }
	scope :created_last_x_days, -> (num_days) { where("created_at>=?", Time.now - (num_days * 86400) ) }
	scope :inserted_last_x_days, -> (num_days) { where("ins_date>=?", Time.now - (num_days * 86400) ) }
	scope :geocode_not_precise, -> { where("location_type != 'ROOFTOP'") }

end

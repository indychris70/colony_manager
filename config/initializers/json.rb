#############
# Monkey patch for Gem json v 1.8
#
# To bypass the deprecation warning encountered during assets:precompile task
#
# As per https://github.com/flori/json/issues/399#issuecomment-734863279
#############

module JSON
    module_function
  
    def parse(source, opts = {})
      Parser.new(source, **opts).parse
    end
  end
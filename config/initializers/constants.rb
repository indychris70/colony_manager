LOCATION_TYPES = {
	'ROOFTOP' => { :color => '#5CB85C', :class => 'success' , :description => 'Indicates that the returned result reflects a precise geocode'},
	'RANGE_INTERPOLATED' => { :color => '#5BC0DE', :class => 'info' , :description => 'Indicates that the returned result reflects an approximation (usually on a road) interpolated between two precise points (such as intersections)'},
	'GEOMETRIC_CENTER' => { :color => '#F0AD4E', :class => 'warning' , :description => 'Indicates that the returned result is the geometric center of a result such as a polyline (for example, a street) or polygon (region)'},
	'APPROXIMATE' => { :color => '#D9534F', :class => 'danger' , :description => 'Indicates that the returned result is approximate'}
}

BASE_COLORS = %w(white black blue caramel chocolate lilac taupe cinnamon fawn red cream apricot)

WHITE_VALUE = ['albino','not albino']

TORTOISESHELL = %w(no distinct brindled)

TABBY = %w(no mackerel classic spotted ticked)

COLORPOINT = %w(no point mink sepia)

WHITESPOTTED = ['no','mitted','tuxedo','mask and mantle','cap and saddle','harlequin','van','magpie','seychellois']

TIPPED = ['no','silver chinchilla','silver shade','silver smoke','golden chinchilla','golden shade','golden smoke']

EYE_COLOR = %w(green hazel gold yellow amber orange copper blue)

EYE_CATEGORY = ["regular","odd","dichroic"]

# colony population events

NEW_COLONY = "new_colony"
TRAPPING_PROJECT = "trapping_project"
STATUS_CHANGE = "status_change"
ASSIGNED_USER_CHANGE = "assigned_user_change"
CAT_ADDED = "cat_added"
CAT_REMOVED = "cat_removed"
CAT_ALTERED = "cat_altered"
UPDATE_SEX_ALTERED = "update_sex_altered"
UPDATE_SEX_NOT_ALTERED = "update_sex_not_altered"

COLONY_POP_EVENTS = {
	NEW_COLONY => "Colony Created",
	TRAPPING_PROJECT => "Trapping Project",
	STATUS_CHANGE => "Status Change",
	ASSIGNED_USER_CHANGE => "Assigned User Change",
	CAT_ADDED => "Cat Added to Colony",
	CAT_REMOVED => "Cat Removed from Colony",
	UPDATE_SEX_ALTERED => "Updated Cat Sex: Altered",
	UPDATE_SEX_NOT_ALTERED => "Updated Cat Sex: Not Altered"
}
Rails.application.routes.draw do

  resources :clinics
  resources :colony_pop_events
  resources :colony_population_event_types
  resources :cat_events
  resources :trapping_projects
  get "begin_finalization/:id" => "trapping_projects#begin_finalization", as: "begin_finalization"
  get "continue_finalization/:id" => "trapping_projects#continue_finalization", as: "continue_finalization"
  post "complete_finalization/:id" => "trapping_projects#complete_finalization", as: "complete_finalization"

  resources :cat_event_outcomes
  resources :cat_event_types
  if ["colony-manager-demo-staging.herokuapp.com","colony-manager-demo.herokuapp.com"].include?(ENV["HOST_URL"]) || Rails.env == "development"
      root "colonies#index"
  else
      root "colonies#map"
  end

  # Devise and Invitables
  devise_for :users, controllers: {
    passwords: 'users/passwords',
    sessions: 'users/sessions',
    registrations: 'users/registrations',
    invitations: 'users/invitations'
  }
  match '/edit_invite_limit', to: 'users#edit_invite_limit', via: :all

  resources :deleted_cat_reasons
  resources :cats

  resources :saved_filters

  resources :categorizations

  resources :categories

  resources :status_codes

  resources :notes

  resources :role_values

  get 'add_user' => 'users#new'
  resources :users do 
    resources :notes
  end
  # get "signin" => "sessions#new"
  get "mass_assign" => "colonies#mass_assign"
  get "manage_duplicates" => "colonies#manage_duplicates"
  get "deleted_colonies" => "colonies#deleted_colonies"
  get "manage_colony_people/:id" => "colonies#manage_colony_people", as: "manage_colony_people"
  post "update_colony_person/colony/:id/person/:person_id/role/:role" => "colonies#update_colony_person", as: "update_colony_person"
  post "update_person_roles/person/:id/role_value_id/:role_value_id" => "people#update_person_roles", as: "update_person_roles"
  get "deleted_people" => "people#deleted_people"
  post "update_deleted_colonies" => "colonies#update_deleted_colonies"
  post "update_deleted_people" => "people#update_deleted_people"
  get "check_addresses" => "colonies#check_addresses"
  # resource :session
  get "dashboard" => "colonies#dashboard"
  get "map" => "colonies#map"
  get "summary_report" => "colonies#summary_report"
  get "quick_create" => "cats#quick_create"
  get "deleted_cats" => "cats#deleted_cats"
  post "update_deleted_cats" => "cats#update_deleted_cats"
  # autocomplete
  get "colonies/autocomplete_colony_formatted_address"
  get "people/autocomplete_person_full_name"
  get "search_selection" => "colonies#search_selection"
  get "person_search_selection" => "people#search_selection"
  get "add_person" => "people#search"
  get "add_colony" => "colonies#search"

  resources :colonies do
    collection { post :import }
    resources :notes
    resources :cats do
      resources :notes
    end
  end

  resources :people

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
